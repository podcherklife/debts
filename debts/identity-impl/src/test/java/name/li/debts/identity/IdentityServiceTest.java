package name.li.debts.identity;

import static com.google.common.truth.Truth.assertThat;
import static com.lightbend.lagom.javadsl.testkit.ServiceTest.defaultSetup;
import static com.lightbend.lagom.javadsl.testkit.ServiceTest.withServer;

import org.junit.Test;

import name.li.debts.commons.test.Tests;
import name.li.debts.identity.api.AcquireTokenRequest;
import name.li.debts.identity.api.CheckPasswordRequest;
import name.li.debts.identity.api.CreateIdentityRequest;
import name.li.debts.identity.api.IdentityService;

public class IdentityServiceTest {
	@Test
	public void shouldCreateIdentity() {
		withServer(defaultSetup().withCassandra(), server -> {
			IdentityService service = server.client(IdentityService.class);

			String id = Tests.get(service.createIdentity()
					.invoke(CreateIdentityRequest.builder().name("user").password("password").build()));
			assertThat(id).isNotNull();
		});
	}

	@Test
	public void shouldCheckPassword() {
		withServer(defaultSetup().withCassandra(), server -> {
			IdentityService service = server.client(IdentityService.class);

			String id = Tests.get(service.createIdentity()
					.invoke(CreateIdentityRequest.builder().name("user").password("password").build()));

			Boolean correctPasswordCheck = Tests.get(service.checkPassword(id)
					.invoke(CheckPasswordRequest.builder().password("password").build()));
			assertThat(correctPasswordCheck).isTrue();

			Boolean incorrectPasswordCheck = Tests.get(service.checkPassword(id)
					.invoke(CheckPasswordRequest.builder().password("incorrectPassword").build()));
			assertThat(incorrectPasswordCheck).isFalse();
		});
	}

	@Test
	public void shouldAcquireToken() {
		withServer(defaultSetup().withCassandra(), server -> {
			IdentityService service = server.client(IdentityService.class);

			String id = Tests.get(service.createIdentity()
					.invoke(CreateIdentityRequest.builder().name("user").password("password").build()));

			String token = Tests.get(service.acquireToken()
					.invoke(AcquireTokenRequest.builder().login("user").password("password").build()));

			assertThat(token).isNotNull();

		});
	}
}

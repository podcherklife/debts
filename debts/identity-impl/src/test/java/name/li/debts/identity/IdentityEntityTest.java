package name.li.debts.identity;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver;
import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver.Outcome;

import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;
import name.li.debts.identity.impl.CheckPassword;
import name.li.debts.identity.impl.CreateIdentity;
import name.li.debts.identity.impl.GetState;
import name.li.debts.identity.impl.IdentityCommand;
import name.li.debts.identity.impl.IdentityEntity;
import name.li.debts.identity.impl.IdentityEvent;
import name.li.debts.identity.impl.IdentityInfo;
import name.li.debts.identity.impl.IdentityState;

public class IdentityEntityTest {
	private static ActorSystem system;

	@BeforeClass
	public static void setup() {

		system = ActorSystem.create("DebtsEntityTest");
	}

	@AfterClass
	public static void teardown() {
		TestKit.shutdownActorSystem(system);
		system = null;
	}

	private PersistentEntityTestDriver<IdentityCommand, IdentityEvent, IdentityState> driver;

	@Before
	public void createDriver() {
		this.driver = new PersistentEntityTestDriver<IdentityCommand, IdentityEvent, IdentityState>(
				system, new IdentityEntity(), "entity1");
	}

	public void assertNoIssues() {
		if (!driver.getAllIssues().isEmpty()) {
			driver.getAllIssues().forEach(System.out::println);
			fail("There were issues " + driver.getAllIssues().get(0));
		}
	}

	@Test
	public void canCreateIdentity() {
		Outcome<IdentityEvent, IdentityState> creationResult = driver.run(
				CreateIdentity.builder().name("someName").password("pass").build());
		assertThat(creationResult.issues()).isEmpty();

		Outcome<IdentityEvent, IdentityState> getStateResult = driver
				.run(GetState.builder().build());

		assertThat(getStateResult.issues()).isEmpty();
		assertThat(getStateResult.getReplies()).hasSize(1);
		assertThat(getStateResult.getReplies().get(0))
				.isEqualTo(IdentityInfo.builder().id("entity1").name("someName").build());

		assertNoIssues();
	}

	@Test
	public void checkPasswordReturnsTrueForValidPassword() {
		driver.run(CreateIdentity.builder().name("someName").password("pass").build());

		Outcome<IdentityEvent, IdentityState> checkPasswordResult = driver
				.run(CheckPassword.builder().password("pass").build());
		assertThat(checkPasswordResult.issues()).isEmpty();
		assertThat(checkPasswordResult.getReplies()).hasSize(1);
		assertThat(checkPasswordResult.getReplies().get(0)).isEqualTo(true);

	}

	@Test
	public void checkPasswordReturnsFalseForInvalidPassword() {
		driver.run(CreateIdentity.builder().name("someName").password("pass").build());

		Outcome<IdentityEvent, IdentityState> checkPasswordResult = driver
				.run(CheckPassword.builder().password("incorrectPass").build());
		assertThat(checkPasswordResult.issues()).isEmpty();
		assertThat(checkPasswordResult.getReplies()).hasSize(1);
		assertThat(checkPasswordResult.getReplies().get(0)).isEqualTo(false);

	}
}

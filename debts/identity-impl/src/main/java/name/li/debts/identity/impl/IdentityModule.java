package name.li.debts.identity.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

import name.li.debts.commons.security.Insecurity;
import name.li.debts.commons.security.TokenGenerator;
import name.li.debts.identity.api.IdentityService;

public class IdentityModule extends AbstractModule implements ServiceGuiceSupport {

	@Override
	protected void configure() {
		bindService(IdentityService.class, IdentityServiceImpl.class);
		bind(TokenGenerator.class).toInstance(Insecurity.INSECURE_TOKEN_GENERATOR());
	}
}

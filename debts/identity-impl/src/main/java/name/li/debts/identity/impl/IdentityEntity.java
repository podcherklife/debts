package name.li.debts.identity.impl;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.apache.curator.shaded.com.google.common.base.Objects;
import org.joda.time.DateTime;

import com.google.common.hash.Hashing;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

public class IdentityEntity extends PersistentEntity<IdentityCommand, IdentityEvent, IdentityState> {

	@Override
	public PersistentEntity<IdentityCommand, IdentityEvent, IdentityState>.Behavior initialBehavior(
			Optional<IdentityState> snapshotState) {

		PersistentEntity<IdentityCommand, IdentityEvent, IdentityState>.BehaviorBuilder b = newBehaviorBuilder(
				snapshotState
						.orElse(IdentityState.builder().name("INITIAL NAME").passwordHash("XXX").salt("FOOOOOOO")
								.build()));

		b.setReadOnlyCommandHandler(GetState.class,
				(cmd, ctx) -> ctx.reply(
						IdentityInfo.builder()
								.name(state().name())
								.id(entityId())
								.build()));

		b.setCommandHandler(CreateIdentity.class,
				(cmd, ctx) -> {
					String password = cmd.password();
					String salt = DateTime.now().toString();
					return ctx.thenPersist(
							IdentityCreated.builder().name(cmd.name()).passwordHash(new String(hash(password, salt)))
									.salt(salt).build(),
							(c) -> ctx.reply(entityId()));
				});

		b.setEventHandler(IdentityCreated.class, evt -> IdentityState.builder().name(evt.name())
				.passwordHash(evt.passwordHash()).salt(evt.salt()).build());

		b.setReadOnlyCommandHandler(CheckPassword.class, (cmd, ctx) -> ctx
				.reply(Objects.equal(new String(hash(cmd.password(), state().salt())), state().passwordHash())));

		return b.build();
	}

	private byte[] hash(String password, String salt) {
		return Hashing.sha256().hashString(password + salt, StandardCharsets.UTF_8).asBytes();
	}

}
package name.li.debts.identity.impl;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lightbend.lagom.serialization.CompressedJsonable;

import name.li.debts.commons.ImmutableStyle;

@Immutable
@ImmutableStyle
@JsonSerialize
public interface AbstractIdentityState extends CompressedJsonable {

	String name();

	String passwordHash();

	String salt();

}

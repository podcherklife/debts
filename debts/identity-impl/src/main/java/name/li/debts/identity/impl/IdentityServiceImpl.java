package name.li.debts.identity.impl;

import javax.inject.Inject;

import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;

import akka.NotUsed;
import name.li.debts.commons.security.TokenGenerator;
import name.li.debts.identity.api.AcquireTokenRequest;
import name.li.debts.identity.api.CheckPasswordRequest;
import name.li.debts.identity.api.CreateIdentityRequest;
import name.li.debts.identity.api.IdentityService;
import name.li.debts.identity.api.QueryStateResponse;

public class IdentityServiceImpl implements IdentityService {

	private final PersistentEntityRegistry persistentEntityRegistry;
	private TokenGenerator tokenGenerator;

	@Inject
	public IdentityServiceImpl(TokenGenerator tokenGenerator, PersistentEntityRegistry persistentEntityRegistry) {
		this.tokenGenerator = tokenGenerator;
		this.persistentEntityRegistry = persistentEntityRegistry;
		persistentEntityRegistry.register(IdentityEntity.class);
	}

	@Override
	public ServiceCall<CreateIdentityRequest, String> createIdentity() {
		return req -> persistentEntityRegistry.refFor(IdentityEntity.class, req.name())
				.ask(CreateIdentity.builder().name(req.name()).password(req.password()).build());
	}

	@Override
	public ServiceCall<NotUsed, QueryStateResponse> getInfo(String id) {
		return (req) -> persistentEntityRegistry.refFor(IdentityEntity.class, id)
				.ask(GetState.builder().build())
				.thenApply(state -> QueryStateResponse.builder().name(state.name()).id(state.id()).build());
	}

	@Override
	public ServiceCall<CheckPasswordRequest, Boolean> checkPassword(String id) {
		return req -> persistentEntityRegistry.refFor(IdentityEntity.class, id)
				.ask(CheckPassword.builder().password(req.password()).build());
	}

	@Override
	public ServiceCall<AcquireTokenRequest, String> acquireToken() {
		return req -> persistentEntityRegistry.refFor(IdentityEntity.class, req.login())
				.ask(CheckPassword.builder().password(req.password()).build())
				.thenApply(passwordIsCorrect -> {
					if (passwordIsCorrect) {
						return tokenGenerator.generateToken(req.login());
					} else {
						throw new RuntimeException("Incorrect password");
					}
				});
	}

}

package name.li.debts.identity.impl;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity.ReplyType;
import com.lightbend.lagom.serialization.Jsonable;

import name.li.debts.commons.ImmutableStyle;

public interface IdentityCommand extends Jsonable {

	@Immutable
	@ImmutableStyle
	@JsonSerialize
	interface AbstractCreateIdentity extends IdentityCommand, ReplyType<String> {
		String name();

		String password();
	}

	@Immutable
	@ImmutableStyle
	@JsonSerialize
	interface AbstractGetState extends IdentityCommand, ReplyType<IdentityInfo> {

	}

	@Immutable
	@ImmutableStyle
	@JsonSerialize
	interface AbstractIdentityInfo extends Jsonable {
		String name();

		String id();
	}

	@Immutable
	@ImmutableStyle
	@JsonSerialize
	interface AbstractCheckPassword extends IdentityCommand, ReplyType<Boolean> {
		String password();
	}

}

package name.li.debts.identity.impl;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;

import name.li.debts.commons.ImmutableStyle;

public interface IdentityEvent extends Jsonable, AggregateEvent<IdentityEvent> {

	AggregateEventShards<IdentityEvent> TAG = AggregateEventTag.sharded(IdentityEvent.class, 4);

	@Immutable
	@ImmutableStyle
	@JsonSerialize
	interface AbstractIdentityCreated extends IdentityEvent {
		String name();

		String passwordHash();

		String salt();
	}

	@Override
	default AggregateEventTagger<IdentityEvent> aggregateTag() {
		return TAG;
	}

}

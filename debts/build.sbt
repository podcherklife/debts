organization in ThisBuild := "name.li"
version in ThisBuild := "1.0-SNAPSHOT"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.12.8"



lazy val `debts` = (project in file("."))
  .aggregate(`debts-api`, `debts-impl`, `identity-api`, `identity-impl`, `thread-impl`, `thread-api`, `commons`)

lazy val `debts-api` = (project in file("debts-api"))
  .settings(common)
  .settings(immutablesSettings)
  .settings(
    libraryDependencies ++= Seq(
      lagomJavadslApi,
      immutables,
      annotationsApi
    )
  )
  .dependsOn(`commons`)

lazy val `debts-impl` = (project in file("debts-impl"))
  .enablePlugins(LagomJava)
  .settings(common)
  .settings(immutablesSettings)
  .settings(
    libraryDependencies ++= Seq(
      lagomJavadslPersistenceCassandra,
      lagomJavadslKafkaBroker,
      lagomLogback,
      lagomJavadslTestKit,
      immutables,
      annotationsApi
    )
  )
  .settings(lagomForkedTestSettings)
  .dependsOn(`debts-api`, `commons`)

lazy val `identity-api` = (project in file("identity-api"))
  .settings(common)
  .settings(immutablesSettings)
  .settings(
    libraryDependencies ++= Seq(
      lagomJavadslApi,
      immutables,
      annotationsApi
    )
  )
  .dependsOn(`commons`)

lazy val `identity-impl` = (project in file("identity-impl"))
  .enablePlugins(LagomJava)
  .settings(common)
  .settings(immutablesSettings)
  .settings(
    libraryDependencies ++= Seq(
      lagomJavadslPersistenceCassandra,
      lagomJavadslKafkaBroker,
      lagomLogback,
      lagomJavadslTestKit,
      immutables,
      annotationsApi
    )
  )
  .settings(lagomForkedTestSettings)
  .dependsOn(`identity-api`, `commons`)

lazy val `thread-api` = (project in file("thread-api"))
  .settings(common)
  .settings(immutablesSettings)
  .settings(
    libraryDependencies ++= Seq(
      lagomJavadslApi,
      immutables,
      annotationsApi
    )
  )
  .dependsOn(`commons`)

lazy val `thread-impl` = (project in file("thread-impl"))
  .enablePlugins(LagomJava)
  .settings(common)
  .settings(immutablesSettings)
  .settings(
    libraryDependencies ++= Seq(
      lagomJavadslPersistenceCassandra,
      lagomJavadslKafkaBroker,
      lagomLogback,
      lagomJavadslTestKit,
      immutables,
      annotationsApi
    )
  )
  .settings(lagomForkedTestSettings)
  .dependsOn(`thread-api`, `commons`)

lazy val `commons` = (project in file("commons"))
  .settings(common)
  .settings(immutablesSettings)
  .settings(
    libraryDependencies ++= Seq(
      immutables,
      annotationsApi,
      jwt,
      guice,
      lagomJavadslApi,
      lagomJavadslServer,
      truth,
    )
  )

val immutables = "org.immutables" % "value" % "2.7.4" 
val annotationsApi = "javax.annotation" % "javax.annotation-api" % "1.2"
val truth =  "com.google.truth" % "truth" % "0.46"
val jwt = "com.auth0" % "java-jwt" % "3.8.1"
val guice = "com.google.inject" % "guice" % "4.2.2"


val immutablesSettings = Seq(
  managedSourceDirectories in Compile += baseDirectory.value / "generated_sources",

  compile in Compile := (compile in Compile).dependsOn(Def.task({
    (baseDirectory.value / "generated_sources").mkdirs()
  })).value,

  // LagomJava overrides javacOptions for compile configuration, so we have to specify "in Compile"
  javacOptions in Compile ++= Seq("-s", baseDirectory.value / "generated_sources" absolutePath),

  cleanFiles += baseDirectory.value / "generated_sources"

)


def common = Seq(
  javaOptions in Test += "-Dlagom.circuit-breaker.default.call-timeout=30s",
  EclipseKeys.projectFlavor := EclipseProjectFlavor.Java
)

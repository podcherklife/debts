package name.li.debts.threads.api;

import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.pathCall;

import java.util.Collection;

import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

import akka.Done;
import akka.NotUsed;
import name.li.debts.commons.security.TokenDecoder;
import name.li.debts.commons.security.TokenHeaderFilter;

public interface ThreadService extends Service {

	ServiceCall<CreateThreadRequest, String> createThread();

	ServiceCall<NotUsed, ThreadDetails> getThreadDetails(String threadId);

	ServiceCall<AddParticipantsRequest, Done> addParticipants(String threadId);

	ServiceCall<String, Done> removeParticipant(String threadId);

	ServiceCall<NotUsed, Done> reopenThread(String threadId);

	ServiceCall<NotUsed, Done> closeThread(String threadId);

	ServiceCall<NotUsed, Collection<ThreadSummary>> getAllUserThreads();

	@Override
	default Descriptor descriptor() {
		return named("threads").withHeaderFilter(new TokenHeaderFilter(new TokenDecoder()))
				.withCalls(
						pathCall("/api/threads/", this::createThread),
						pathCall("/api/threads/:threadId", this::getThreadDetails),
						pathCall("/api/threads/:threadId", this::removeParticipant),
						pathCall("/api/threads/:threadId/open", this::reopenThread),
						pathCall("/api/threads/:threadId/close", this::closeThread),
						pathCall("/api/myThreads", this::getAllUserThreads),
						pathCall("/api/threads/:threadId/addParticipants", this::addParticipants))
				.withAutoAcl(true);
	}

}

package name.li.debts.threads.api;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import name.li.debts.commons.ImmutableStyle;

@JsonDeserialize
@Immutable
@ImmutableStyle
public interface AbstractRecordParticipation {

	String userId();

	int sum();

}

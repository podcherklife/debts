package name.li.debts.threads.api;

import java.util.Collections;
import java.util.Set;

import org.immutables.value.Value.Default;
import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import name.li.debts.commons.ImmutableStyle;

@JsonDeserialize
@Immutable
@ImmutableStyle
public interface AbstractCreateThreadRequest {

	String name();

	String description();

	@Default
	default Set<String> participants() {
		return Collections.emptySet();
	}

}

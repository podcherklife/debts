package name.li.debts.threads.api;

import java.util.Collection;

import org.immutables.value.Value.Default;
import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import name.li.debts.commons.ImmutableStyle;

@JsonDeserialize
@Immutable
@ImmutableStyle
public interface AbstractThreadRecord {

	String id();

	String name();

	@Default
	default String description() {
		return "";
	}

	Collection<RecordParticipation> partitipants();

}

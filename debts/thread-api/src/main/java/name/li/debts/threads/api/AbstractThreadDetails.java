package name.li.debts.threads.api;

import java.util.List;
import java.util.Set;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import name.li.debts.commons.ImmutableStyle;

@JsonDeserialize
@Immutable
@ImmutableStyle
public interface AbstractThreadDetails {

	String id();

	String name();

	String description();

	Set<String> participants();

	List<ThreadRecord> records();

	ThreadStatus status();

}

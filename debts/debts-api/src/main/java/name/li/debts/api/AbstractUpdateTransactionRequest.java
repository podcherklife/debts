package name.li.debts.api;

import java.util.Optional;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import name.li.debts.commons.ImmutableStyle;

@JsonDeserialize
@Immutable
@ImmutableStyle
public interface AbstractUpdateTransactionRequest {

	String requestor();

	Optional<String> from();

	Optional<String> to();

	Optional<Integer> sum();

}

package name.li.debts.api;

import java.time.LocalDateTime;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import name.li.debts.commons.ImmutableStyle;

@JsonDeserialize
@Immutable
@ImmutableStyle
public interface AbstractTransactionSummary {

	String id();

	String from();

	String to();

	int sum();

	LocalDateTime creationDate();

}

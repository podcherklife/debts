package name.li.debts.api;

import java.util.Collection;
import java.util.Optional;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import name.li.debts.commons.ImmutableStyle;

@JsonDeserialize
@Immutable
@ImmutableStyle
public interface AbstractGetUserTransactionsResponse {

	Collection<TransactionSummary> transactions();

	Optional<String> pageToken();

}

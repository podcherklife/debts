package name.li.debts.api.data;

import org.immutables.value.Value;
import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Immutable
@JsonDeserialize
@Value.Style(typeAbstract = "Abstract*", typeImmutable = "*")
public interface AbstractComment {

	String text();

	String timestamp();
}

package name.li.debts.api;

import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.pathCall;

import java.util.Optional;

import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

import akka.Done;
import akka.NotUsed;
import name.li.debts.commons.security.TokenDecoder;
import name.li.debts.commons.security.TokenHeaderFilter;

public interface TransactionsService extends Service {

	/**
	 * Returns id of created transaction 
	 */
	public ServiceCall<CreateTransactionRequest, String> createTransaction();

	public ServiceCall<UpdateTransactionRequest, Done> updateTransaction(String id);

	public ServiceCall<NotUsed, TransactionDetails> getTransactionState(String id);

	public ServiceCall<NotUsed, GetUserTransactionsResponse> getUserTransactions(
			String userId,
			Optional<String> pageToken);

	@Override
	default Descriptor descriptor() {
		return named("debts").withHeaderFilter(new TokenHeaderFilter(new TokenDecoder()))
				.withCalls(
						pathCall("/api/debts/transactions/:userId/page/?pageToken", this::getUserTransactions),
						pathCall("/api/debts/transaction/create", this::createTransaction),
						pathCall("/api/debts/transaction/:id/update", this::updateTransaction),
						pathCall("/api/debts/transaction/:id", this::getTransactionState))
				.withAutoAcl(true);
	}
}

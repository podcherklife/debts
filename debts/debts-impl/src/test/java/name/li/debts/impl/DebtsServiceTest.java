package name.li.debts.impl;

import static com.google.common.truth.Truth.assertThat;
import static com.lightbend.lagom.javadsl.testkit.ServiceTest.defaultSetup;
import static com.lightbend.lagom.javadsl.testkit.ServiceTest.withServer;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.function.Function;

import org.junit.Test;

import com.google.common.collect.Lists;

import name.li.debts.api.CreateTransactionRequest;
import name.li.debts.api.GetUserTransactionsResponse;
import name.li.debts.api.TransactionDetails;
import name.li.debts.api.TransactionSummary;
import name.li.debts.api.TransactionsService;
import name.li.debts.api.UpdateTransactionRequest;
import name.li.debts.commons.security.AuthenticatedDebtPrincipal;
import name.li.debts.commons.security.Insecurity;
import name.li.debts.commons.security.Security;
import name.li.debts.commons.security.TokenDecoder;
import name.li.debts.commons.security.TokenGenerator;
import name.li.debts.commons.security.TokenValidator;
import name.li.debts.commons.test.Tests;

public class DebtsServiceTest {

	TokenGenerator tokenGenerator = Insecurity.INSECURE_TOKEN_GENERATOR();
	TokenDecoder tokenDecoder = new TokenDecoder();
	TokenValidator tokenValidator = Insecurity.INSECURE_TOKEN_VALIDATOR();

	Function<String, AuthenticatedDebtPrincipal> generatePrincipal = user -> {
		String rawToken = tokenGenerator.generateToken(user);
		return tokenDecoder.decodePrincipal(rawToken).validate(tokenValidator);
	};

	AuthenticatedDebtPrincipal john = generatePrincipal.apply("John");
	AuthenticatedDebtPrincipal sam = generatePrincipal.apply("Sam");
	AuthenticatedDebtPrincipal richard = generatePrincipal.apply("Richard");

	@Test
	public void happyFlow() {
		withServer(defaultSetup().withCassandra(), server -> {
			TransactionsService service = server.client(TransactionsService.class);

			String id = Tests.get(service.createTransaction()
					.handleRequestHeader(Security.authenticated(john))
					.invoke(CreateTransactionRequest.builder().requestor("John").from("John").to("Richard").sum(100)
							.build()));
			assertThat(id).isNotNull();

			TransactionDetails initialState = Tests.get(service.getTransactionState(id)
					.handleRequestHeader(Security.authenticated(richard))
					.invoke());

			assertThat(initialState.creator()).isEqualTo("John");
			assertThat(initialState.from()).isEqualTo("John");
			assertThat(initialState.to()).isEqualTo("Richard");
			assertThat(initialState.sum()).isEqualTo(100);

			Tests.get(service.updateTransaction(id)
					.handleRequestHeader(Security.authenticated(john))
					.invoke(UpdateTransactionRequest.builder().requestor("Sam").to("Sam").from("Sally").sum(10)
							.build()));

			TransactionDetails updatedState = Tests.get(service.getTransactionState(id)
					.handleRequestHeader(Security.authenticated(sam))
					.invoke());

			assertThat(updatedState.creator()).isEqualTo("John");
			assertThat(updatedState.from()).isEqualTo("Sally");
			assertThat(updatedState.to()).isEqualTo("Sam");
			assertThat(updatedState.sum()).isEqualTo(10);
		});
	}

	@Test
	public void shouldListTransactionsInvolvingUser() {
		withServer(defaultSetup().withCassandra(), server -> {
			TransactionsService service = server.client(TransactionsService.class);

			String id1 = Tests.get(service.createTransaction()
					.handleRequestHeader(Security.authenticated(john))
					.invoke(CreateTransactionRequest.builder().requestor("John").from("John").to("Richard").sum(100)
							.build()));

			TransactionDetails transaction1 = Tests.get(service.getTransactionState(id1)
					.handleRequestHeader(Security.authenticated(john))
					.invoke());

			String id2 = Tests.get(service.createTransaction()
					.handleRequestHeader(Security.authenticated(john))
					.invoke(CreateTransactionRequest.builder().requestor("John").from("John").to("Sam").sum(200)
							.build()));

			TransactionDetails transaction2 = Tests.get(service.getTransactionState(id2)
					.handleRequestHeader(Security.authenticated(john))
					.invoke());

			Tests.eventually(
					() -> {
						GetUserTransactionsResponse resp = Tests
								.get(service.getUserTransactions("John", Optional.empty())
										.handleRequestHeader(Security.authenticated(john))
										.invoke());
						if (resp.transactions().isEmpty()) {
							return Tests.CallResult.noData();
						} else {
							return Tests.CallResult.done(resp.transactions());
						}
					}, data -> assertThat(data)
							.containsExactly(
									TransactionSummary.builder()
											.id(transaction1.id())
											.from("John")
											.to("Richard")
											.creationDate(transaction1.creationDate())
											.sum(100)
											.build(),
									TransactionSummary.builder()
											.id(transaction2.id())
											.from("John")
											.to("Sam")
											.creationDate(transaction2.creationDate())
											.sum(200)
											.build()));
		});
	}

	@Test
	public void shouldSupportPaginationForTransactions() {
		withServer(defaultSetup().withCassandra(), server -> {
			TransactionsService service = server.client(TransactionsService.class);
			int pageSize = 100;

			int firstPageCounter = pageSize;
			ArrayList<TransactionDetails> firstPageTransactions = Lists.newArrayList();
			while (firstPageCounter-- > 0) {
				createRandomTransaction(service, john);
			}

			ArrayList<TransactionDetails> secondPageTransactions = Lists.newArrayList(
					createRandomTransaction(service, john),
					createRandomTransaction(service, john),
					createRandomTransaction(service, john));

			Tests.sleep(10);

			GetUserTransactionsResponse firstPage = Tests
					.get(service.getUserTransactions(john.getName(), Optional.empty())
							.handleRequestHeader(Security.authenticated(john))
							.invoke());

			Optional<String> pageToken = firstPage.pageToken();
			assertThat(pageToken.isPresent()).isTrue();
			GetUserTransactionsResponse secondPage = Tests.get(
					service.getUserTransactions(john.getName(), pageToken)
							.handleRequestHeader(Security.authenticated(john))
							.invoke());

			assertThat(secondPage.transactions()).hasSize(3);
			assertThat(secondPage.transactions().stream().map(TransactionSummary::id).collect(toList()))
					.containsExactly(secondPageTransactions.stream().map(TransactionDetails::id).toArray());

		});
	}

	private TransactionDetails createRandomTransaction(TransactionsService service,
			AuthenticatedDebtPrincipal createdBy) {

		String id = Tests.get(service.createTransaction()
				.handleRequestHeader(Security.authenticated(createdBy))
				.invoke(CreateTransactionRequest.builder()
						.requestor(createdBy.getName())
						.from(createdBy.getName())
						.to("to " + UUID.randomUUID())
						.sum(new Random().nextInt())
						.build()));

		return Tests.get(service.getTransactionState(id)
				.handleRequestHeader(Security.authenticated(createdBy))
				.invoke());
	}

}

package name.li.debts.impl;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver;
import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver.Outcome;
import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver.UnhandledCommand;

import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;
import name.li.debts.commons.entity.Exceptions;
import name.li.debts.impl.entity.CreateTransaction;
import name.li.debts.impl.entity.TransactionCommand;
import name.li.debts.impl.entity.TransactionEntity;
import name.li.debts.impl.entity.TransactionEvent;
import name.li.debts.impl.entity.TransactionState;
import name.li.debts.impl.entity.UpdateTransaction;

public class DebtsEntityTest {
	private static ActorSystem system;

	@BeforeClass
	public static void setup() {
		system = ActorSystem.create("DebtsEntityTest");
	}

	@AfterClass
	public static void teardown() {
		TestKit.shutdownActorSystem(system);
		system = null;
	}

	private PersistentEntityTestDriver<TransactionCommand, TransactionEvent, TransactionState> driver;

	@Before
	public void createDriver() {
		this.driver = new PersistentEntityTestDriver<TransactionCommand, TransactionEvent, TransactionState>(
				system, new TransactionEntity(), "world-1");
	}

	public void assertNoIssues() {
		if (!driver.getAllIssues().isEmpty()) {
			driver.getAllIssues().forEach(System.out::println);
			fail("There were issues " + driver.getAllIssues().get(0));
		}
	}

	@Test
	public void testCannotUpdateNonExistantTransaction() {
		Outcome<TransactionEvent, TransactionState> result = driver
				.run(UpdateTransaction.builder().from("borrower").updatedBy("random").build());
		assertThat(result.issues()).hasSize(1);
		assertThat(result.issues().get(0)).isInstanceOf(UnhandledCommand.class);
	}

	@Test
	public void OnlyAuthorCanUpdateTransaction() {

		Outcome<TransactionEvent, TransactionState> creationResult = driver.run(
				CreateTransaction.builder().creator("creator").from("lender").to("borrower").sum(10).build());
		assertThat(creationResult.issues()).isEmpty();

		Outcome<TransactionEvent, TransactionState> creatorUpdates = driver
				.run(UpdateTransaction.builder().from("otherBorrower").updatedBy("creator").build());
		assertThat(creatorUpdates.issues()).isEmpty();

		Outcome<TransactionEvent, TransactionState> nonCreatorUpdates = driver
				.run(UpdateTransaction.builder().from("otherBorrower").updatedBy("noNcreator").build());
		assertThat(nonCreatorUpdates.getReplies().get(0)).isInstanceOf(Exceptions.UnauthorizedCommandException.class);

		assertNoIssues();
	}

}

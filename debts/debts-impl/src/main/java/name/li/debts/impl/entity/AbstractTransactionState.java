package name.li.debts.impl.entity;

import java.time.LocalDateTime;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lightbend.lagom.serialization.CompressedJsonable;

import name.li.debts.commons.ImmutableStyle;

@Immutable
@ImmutableStyle
@JsonSerialize
public interface AbstractTransactionState extends CompressedJsonable {

	String from();

	String to();

	String creator();

	LocalDateTime creationDate();

	int sum();

}

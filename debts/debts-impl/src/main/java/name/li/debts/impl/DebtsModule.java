package name.li.debts.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

import name.li.debts.api.TransactionsService;
import name.li.debts.commons.security.Insecurity;
import name.li.debts.commons.security.TokenValidator;

/**
 * The module that binds the DebtsService so that it can be served.
 */
public class DebtsModule extends AbstractModule implements ServiceGuiceSupport {
	@Override
	protected void configure() {
		bindService(TransactionsService.class, TransactionsServiceImpl.class);
		bind(TokenValidator.class).toInstance(Insecurity.INSECURE_TOKEN_VALIDATOR());
	}
}

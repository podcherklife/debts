package name.li.debts.impl.entity;

import java.util.Optional;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import com.lightbend.lagom.serialization.CompressedJsonable;
import com.lightbend.lagom.serialization.Jsonable;

import akka.Done;
import name.li.debts.commons.ImmutableStyle;

public interface TransactionCommand extends Jsonable {

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractCreateTransaction
			extends TransactionCommand, CompressedJsonable, PersistentEntity.ReplyType<String> {

		String creator();

		String from();

		String to();

		int sum();

	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractUpdateTransaction
			extends TransactionCommand, CompressedJsonable, PersistentEntity.ReplyType<Done> {

		String updatedBy();

		Optional<String> from();

		Optional<String> to();

		Optional<Integer> sum();

	}

	@Immutable
	@ImmutableStyle
	@JsonDeserialize
	interface AbstractGetCurrentState
			extends TransactionCommand, CompressedJsonable, PersistentEntity.ReplyType<TransactionState> {

		String requestor();

	}

}

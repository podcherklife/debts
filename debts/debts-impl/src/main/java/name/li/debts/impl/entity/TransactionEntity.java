package name.li.debts.impl.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

import akka.Done;
import name.li.debts.commons.entity.Exceptions;

public class TransactionEntity
		extends PersistentEntity<TransactionCommand, TransactionEvent, TransactionState> {

	@Override
	public Behavior initialBehavior(Optional<TransactionState> snapshotState) {
		BehaviorBuilder b = newBehaviorBuilder(
				snapshotState.orElse(TransactionState.builder()
						.from("INITIAL BORROWER")
						.to("INITIAL LENDER")
						.sum(-1)
						.creator("INITIAL CREATOR")
						.creationDate(LocalDateTime.now().minusYears(10000))
						.build()));

		b.setCommandHandler(CreateTransaction.class,
				(cmd, ctx) -> ctx.thenPersistAll(() -> ctx.reply(entityId()),
						TransactionCreated.builder()
								.id(entityId())
								.creator(cmd.creator())
								.creationDate(LocalDateTime.now())
								.from(cmd.from())
								.to(cmd.to())
								.sum(cmd.sum())
								.build()));

		b.setEventHandlerChangingBehavior(TransactionCreated.class,
				(evt) -> createdBehavior(
						TransactionState.builder()
								.from(evt.from())
								.to(evt.to())
								.creationDate(evt.creationDate())
								.creator(evt.creator())
								.sum(evt.sum())
								.build()));

		return b.build();
	}

	private Behavior createdBehavior(TransactionState state) {
		BehaviorBuilder b = newBehaviorBuilder(state);

		b.setCommandHandler(UpdateTransaction.class, (cmd, ctx) -> {
			if (cmd.updatedBy().equals(state.creator())) {
				ArrayList<TransactionEvent> events = new ArrayList<>();
				cmd.to().ifPresent(to -> events.add(ToModified.builder().to(to).build()));
				cmd.from().ifPresent(from -> events.add(FromModified.builder().from(from).build()));
				cmd.sum().ifPresent(sum -> events.add(SumModified.builder().sum(sum).build()));
				return ctx.thenPersistAll(events, () -> ctx.reply(Done.done()));
			} else {
				ctx.commandFailed(
						new Exceptions.UnauthorizedCommandException("Only author can modify transaction"));
				return ctx.done();
			}
		});

		b.setEventHandler(ToModified.class, evt -> state().withTo(evt.to()));
		b.setEventHandler(FromModified.class, evt -> state().withFrom(evt.from()));
		b.setEventHandler(SumModified.class, evt -> state().withSum(evt.sum()));

		b.setReadOnlyCommandHandler(GetCurrentState.class,
				(cmd, ctx) -> ctx.reply(state()));
		return b.build();
	}

}

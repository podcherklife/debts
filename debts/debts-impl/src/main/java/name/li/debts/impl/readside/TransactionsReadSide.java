package name.li.debts.impl.readside;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.pcollections.PSequence;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PagingState;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSide;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;

import akka.Done;
import name.li.debts.api.GetUserTransactionsResponse;
import name.li.debts.api.TransactionSummary;
import name.li.debts.impl.entity.TransactionCreated;
import name.li.debts.impl.entity.TransactionEvent;

public class TransactionsReadSide {

	private final int PAGE_SIZE = 100;

	private CassandraSession cassandra;

	@Inject
	public TransactionsReadSide(CassandraSession cassandra) {
		this.cassandra = cassandra;
	}

	public CompletionStage<GetUserTransactionsResponse> getAllUserTransactions(String user,
			Optional<String> pageToken) {
		return cassandra
				.prepare("SELECT transactionId, fromUser, toUser, sum, interestedUser, creationDate"
						+ " FROM transactionsBetweenUsers WHERE interestedUser = ?")
				.thenApply(pst -> {
					BoundStatement st = pst.bind();
					st.setFetchSize(PAGE_SIZE);
					st.bind(user);
					pageToken.map(PagingState::fromString).ifPresent(st::setPagingState);
					return st;
				})
				.thenCompose(stm -> cassandra
						.underlying().thenApply(session -> {
							ResultSet result = session.execute(stm);

							Optional<String> pagingState = Optional
									.ofNullable(result.getExecutionInfo().getPagingState())
									.map(PagingState::toString);

							List<TransactionSummary> items = result.all().stream()
									.map(this::makeTransactionSummaryFromRow)
									.collect(Collectors.toList());

							return GetUserTransactionsResponse.builder().transactions(items)
									.pageToken(pagingState).build();
						}));
	}

	private TransactionSummary makeTransactionSummaryFromRow(Row row) {
		return TransactionSummary.builder()
				.id(row.getString("transactionId"))
				.from(row.getString("fromUser"))
				.to(row.getString("toUser"))
				.creationDate(fromDbDate(row.getTimestamp("creationDate")))
				.sum(row.getInt("sum"))
				.build();
	}

	public static class TransactionEventProcessor extends ReadSideProcessor<TransactionEvent> {

		private final CassandraSession cassandra;
		private final CassandraReadSide readSide;
		private PreparedStatement insertTransaction;

		@Inject
		public TransactionEventProcessor(CassandraSession cassandra, CassandraReadSide readSide) {
			this.cassandra = cassandra;
			this.readSide = readSide;
		}

		@Override
		public ReadSideProcessor.ReadSideHandler<TransactionEvent> buildHandler() {
			return readSide
					.<TransactionEvent>builder("transactionoffset")
					.setGlobalPrepare(() -> {
						return cassandra.executeCreateTable("CREATE TABLE IF NOT EXISTS transactionsBetweenUsers("
								+ "interestedUser text," // XXX: switch to uuid
								+ "transactionId text,"
								+ "creationDate timestamp,"
								+ "fromUser text,"
								+ "toUser text,"
								+ "sum int,"
								+ "PRIMARY KEY (interestedUser, creationDate, transactionId)"
								+ ")");
					})
					.setPrepare(tag -> cassandra
							.prepare(
									"INSERT INTO transactionsBetweenUsers(interestedUser, transactionId, fromUser, toUser, sum, creationDate)"
											+ " VALUES (?,?,?,?,?,?)")
							.thenApply(st -> {
								this.insertTransaction = st;
								return Done.done();
							}))
					.setEventHandler(TransactionCreated.class, evt -> {
						return CompletableFuture.completedFuture(Arrays.asList(
								insertTransaction.bind(
										evt.from(),
										evt.id(),
										evt.from(),
										evt.to(),
										evt.sum(),
										toDbDate(evt.creationDate())),
								insertTransaction.bind(
										evt.to(),
										evt.id(),
										evt.from(),
										evt.to(),
										evt.sum(),
										toDbDate(evt.creationDate()))));
					})
					.build();
		}

		@Override
		public PSequence<AggregateEventTag<TransactionEvent>> aggregateTags() {
			return TransactionEvent.TAG.allTags();
		}
	}

	private static LocalDateTime fromDbDate(Date date) {
		return LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC);
	}

	private static Date toDbDate(LocalDateTime localDateTime) {
		return new Date(localDateTime.toInstant(ZoneOffset.UTC).toEpochMilli());
	}
}

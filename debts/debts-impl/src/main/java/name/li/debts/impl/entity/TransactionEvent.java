package name.li.debts.impl.entity;

import java.time.LocalDateTime;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;

import name.li.debts.commons.ImmutableStyle;

public interface TransactionEvent extends Jsonable, AggregateEvent<TransactionEvent> {

	AggregateEventShards<TransactionEvent> TAG = AggregateEventTag.sharded(TransactionEvent.class, 4);

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractTransactionCreated extends TransactionEvent {
		String id();

		String creator();

		LocalDateTime creationDate();

		String from();

		String to();

		int sum();
	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractToModified extends TransactionEvent {
		String to();
	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractFromModified extends TransactionEvent {
		String from();
	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractSumModified extends TransactionEvent {
		int sum();
	}

	@Override
	default AggregateEventTagger<TransactionEvent> aggregateTag() {
		return TAG;
	}
}

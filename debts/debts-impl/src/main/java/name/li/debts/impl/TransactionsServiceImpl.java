package name.li.debts.impl;

import java.util.Optional;
import java.util.UUID;

import javax.inject.Inject;

import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.persistence.ReadSide;

import akka.Done;
import akka.NotUsed;
import name.li.debts.api.CreateTransactionRequest;
import name.li.debts.api.TransactionsService;
import name.li.debts.api.GetUserTransactionsResponse;
import name.li.debts.api.TransactionDetails;
import name.li.debts.api.UpdateTransactionRequest;
import name.li.debts.commons.security.Security;
import name.li.debts.commons.security.TokenValidator;
import name.li.debts.impl.entity.CreateTransaction;
import name.li.debts.impl.entity.GetCurrentState;
import name.li.debts.impl.entity.TransactionCommand;
import name.li.debts.impl.entity.TransactionEntity;
import name.li.debts.impl.entity.UpdateTransaction;
import name.li.debts.impl.readside.TransactionsReadSide;
import name.li.debts.impl.readside.TransactionsReadSide.TransactionEventProcessor;

/**
 * Implementation of the DebtsService.
 */
public class TransactionsServiceImpl implements TransactionsService {
	private final PersistentEntityRegistry persistentEntityRegistry;
	private final TokenValidator tokenValidator;
	private TransactionsReadSide transactions;

	@Inject
	public TransactionsServiceImpl(
			TokenValidator tokenValidator,
			PersistentEntityRegistry persistentEntityRegistry,
			ReadSide readside,
			TransactionsReadSide transactions) {
		this.persistentEntityRegistry = persistentEntityRegistry;
		this.tokenValidator = tokenValidator;
		this.transactions = transactions;

		persistentEntityRegistry.register(TransactionEntity.class);
		readside.register(TransactionEventProcessor.class);
	}

	@Override
	public ServiceCall<CreateTransactionRequest, String> createTransaction() {
		return Security.authenticated(tokenValidator, (principal, request) -> {
			String id = UUID.randomUUID().toString();
			PersistentEntityRef<TransactionCommand> ref = persistentEntityRegistry.refFor(TransactionEntity.class, id);
			return ref.ask(
					CreateTransaction.builder()
							.from(request.from())
							.to(request.to())
							.sum(request.sum())
							.creator(principal.getName()).build());
		});
	}

	@Override
	public ServiceCall<UpdateTransactionRequest, Done> updateTransaction(String id) {
		return Security.authenticated(tokenValidator, (principal, request) -> {
			PersistentEntityRef<TransactionCommand> ref = persistentEntityRegistry.refFor(TransactionEntity.class, id);
			return ref.ask(UpdateTransaction.builder()
					.from(request.from())
					.to(request.to())
					.sum(request.sum())
					.updatedBy(principal.getName())
					.build());
		});
	}

	@Override
	public ServiceCall<NotUsed, TransactionDetails> getTransactionState(String id) {
		return Security.authenticated(tokenValidator, (principal, request) -> {
			PersistentEntityRef<TransactionCommand> ref = persistentEntityRegistry.refFor(TransactionEntity.class, id);
			return ref.ask(GetCurrentState.builder().requestor(principal.getName()).build())
					.thenApply(state -> TransactionDetails.builder()
							.id(id)
							.creator(state.creator())
							.creationDate(state.creationDate())
							.from(state.from())
							.to(state.to())
							.sum(state.sum())
							.build());
		});
	}

	@Override
	public ServiceCall<NotUsed, GetUserTransactionsResponse> getUserTransactions(
			String userId,
			Optional<String> pageToken) {
		return Security.authenticated(tokenValidator, (principal, request) -> {
			return transactions.getAllUserTransactions(principal.getName(), pageToken);
		});
	}
}

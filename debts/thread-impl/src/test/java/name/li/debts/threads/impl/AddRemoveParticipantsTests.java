package name.li.debts.threads.impl;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;

import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver.Outcome;

import name.li.debts.commons.entity.Exceptions;

public class AddRemoveParticipantsTests extends AbstractThreadTest {

	@Test
	public void shouldAddParticipants() {
		createThread("creator");

		Outcome<ThreadEvent, ThreadState> participantAddedResult = driver.run(AddParticipants.builder()
				.requestor("creator")
				.addParticipants("participant").build());
		assertNoIssues();

		assertThat(participantAddedResult.state().participants())
				.containsExactly("creator", "participant");
	}

	@Test
	public void testNonParticipantsCannotAddNewParticipants() {
		createThread("creator");

		Outcome<ThreadEvent, ThreadState> commandResult = driver.run(AddParticipants.builder()
				.requestor("non-participant")
				.addParticipants("non-participant").build());

		assertThat(commandResult.state().participants()).containsExactly("creator");
		assertThat(commandResult.getReplies()).hasSize(1);
		assertThat(commandResult.getReplies().get(0)).isInstanceOf(Exceptions.UnauthorizedCommandException.class);
	}

	@Test
	public void testNonCreatorCannotRemoveOthers() {
		createThread("creator", "p1", "p2");

		Outcome<ThreadEvent, ThreadState> removeOtherResult = driver.run(RemoveParticipant.builder()
				.requestor("p1")
				.participantToRemove("p2").build());

		assertThat(removeOtherResult.getReplies()).hasSize(1);
		assertThat(removeOtherResult.getReplies().get(0)).isInstanceOf(Exceptions.UnauthorizedCommandException.class);
		assertThat(removeOtherResult.state().participants()).containsExactly("creator", "p1", "p2");

	}

	@Test
	public void testNonCreatorCanRemoveSelf() {
		createThread("creator", "p1", "p2");

		Outcome<ThreadEvent, ThreadState> removeSelfResult = driver.run(RemoveParticipant.builder()
				.requestor("p2")
				.participantToRemove("p2").build());

		assertNoIssues();
		assertThat(removeSelfResult.state().participants()).containsExactly("creator", "p1");
	};

	@Test
	public void testCreatorCanRemoveOthers() {
		createThread("creator", "p1", "p2");

		Outcome<ThreadEvent, ThreadState> removeResult = driver.run(RemoveParticipant.builder()
				.requestor("creator")
				.participantToRemove("p1").build());

		assertNoIssues();
		assertThat(removeResult.state().participants()).containsExactly("creator", "p2");
	}
}

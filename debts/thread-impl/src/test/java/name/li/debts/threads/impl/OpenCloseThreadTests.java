package name.li.debts.threads.impl;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;

import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver.Outcome;

import name.li.debts.commons.entity.Exceptions;
import name.li.debts.threads.api.ThreadStatus;

public class OpenCloseThreadTests extends AbstractThreadTest {

	@Test
	public void testOpenThreadCannotBeReopened() {
		createThread("creator");

		Outcome<ThreadEvent, ThreadState> commandResult = reopenThread("creator");

		expectExceptionAndThreadStatus(commandResult, Exceptions.IllegalCommandException.class, ThreadStatus.OPEN);
		expectNoEvents(commandResult);
	}

	@Test
	public void testClosedThreadCannotBeClosedAgain() {
		createThread("creator");
		closeThread("creator");

		Outcome<ThreadEvent, ThreadState> commandResult = closeThread("creator");

		expectExceptionAndThreadStatus(commandResult, Exceptions.IllegalCommandException.class, ThreadStatus.CLOSED);
		expectNoEvents(commandResult);
	}

	@Test
	public void testNonParticipantCannotReopenThread() {
		createThread("creator");
		closeThread("creator");

		Outcome<ThreadEvent, ThreadState> commandResult = reopenThread("nonparticipant");

		expectUnauthorisedExceptionAndThreadStatus(commandResult, ThreadStatus.CLOSED);
		expectNoEvents(commandResult);
	}

	@Test
	public void testParticipantCannotReopenThread() {
		createThread("creator", "participant");
		closeThread("creator");

		Outcome<ThreadEvent, ThreadState> commandResult = reopenThread("participant");

		expectUnauthorisedExceptionAndThreadStatus(commandResult, ThreadStatus.CLOSED);
		expectNoEvents(commandResult);
	}

	@Test
	public void testCreatorCanReopenThread() {
		createThread("creator");
		closeThread("creator");

		Outcome<ThreadEvent, ThreadState> commandResult = reopenThread("creator");

		assertNoIssues();
		assertThat(commandResult.state().status()).isEqualTo(ThreadStatus.OPEN);
		expectReopenedEvent(commandResult, "creator");

	}

	@Test
	public void testCreatorCanCloseThread() {
		createThread("creator", "participant1");

		Outcome<ThreadEvent, ThreadState> commandResult = closeThread("creator");

		assertNoIssues();
		assertThat(commandResult.state().status()).isEqualTo(ThreadStatus.CLOSED);
		expectClosedEvent(commandResult, "creator");
	}

	@Test
	public void testNonCreatorParticipantCannotCloseThread() {
		createThread("creator", "participant1");

		Outcome<ThreadEvent, ThreadState> commandResult = closeThread("participant1");

		expectUnauthorisedExceptionAndThreadStatus(commandResult, ThreadStatus.OPEN);
		expectNoEvents(commandResult);
	}

	@Test
	public void testNonParticipantCannotCloseThread() {
		createThread("creator", "participant1");

		Outcome<ThreadEvent, ThreadState> commandResult = closeThread("nonparticipant");

		expectUnauthorisedExceptionAndThreadStatus(commandResult, ThreadStatus.OPEN);
		expectNoEvents(commandResult);
	}

	private void expectUnauthorisedExceptionAndThreadStatus(Outcome<ThreadEvent, ThreadState> commandResult,
			ThreadStatus status) {
		expectExceptionAndThreadStatus(commandResult, Exceptions.UnauthorizedCommandException.class, status);
	}

	private void expectExceptionAndThreadStatus(
			Outcome<ThreadEvent, ThreadState> commandResult,
			Class<? extends Throwable> exceptionClass, ThreadStatus status) {
		assertThat(commandResult.getReplies()).hasSize(1);
		assertThat(commandResult.getReplies().get(0)).isInstanceOf(exceptionClass);
		assertThat(commandResult.state().status()).isEqualTo(status);
	}

	private void expectClosedEvent(Outcome<ThreadEvent, ThreadState> commandResult, String closedBy) {
		assertThat(commandResult.events()).containsExactly(
				ThreadClosed.builder()
						.threadId(DEFAULT_THREAD_ID)
						.closedBy(closedBy)
						.build());
	}

	private void expectReopenedEvent(Outcome<ThreadEvent, ThreadState> commandResult, String reopenedBy) {
		assertThat(commandResult.events()).containsExactly(
				ThreadReopened.builder()
						.threadId(DEFAULT_THREAD_ID)
						.reopenedBy(reopenedBy).build());
	}

	private void expectNoEvents(Outcome<ThreadEvent, ThreadState> commandResult) {
		assertThat(commandResult.events()).isEmpty();
	}

	private Outcome<ThreadEvent, ThreadState> closeThread(String userWhoClosesThread) {
		return driver.run(CloseThread.builder()
				.requestor(userWhoClosesThread)
				.build());
	}

	private Outcome<ThreadEvent, ThreadState> reopenThread(String userWhoReopensThread) {
		return driver.run(ReopenThread.builder()
				.requestor(userWhoReopensThread)
				.build());
	}
}

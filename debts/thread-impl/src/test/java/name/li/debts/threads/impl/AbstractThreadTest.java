package name.li.debts.threads.impl;

import static org.junit.Assert.fail;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver;
import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver.Outcome;

import akka.actor.ActorSystem;
import akka.testkit.javadsl.TestKit;

public abstract class AbstractThreadTest {
	protected static ActorSystem system;

	protected static final String DEFAULT_THREAD_ID = "threadId";

	@BeforeClass
	public static void setup() {
		system = ActorSystem.create("ThreadEntityTest");
	}

	@AfterClass
	public static void teardown() {
		TestKit.shutdownActorSystem(system);
		system = null;
	}

	protected PersistentEntityTestDriver<ThreadCommand, ThreadEvent, ThreadState> driver;

	@Before
	public void createDriver() {
		this.driver = new PersistentEntityTestDriver<ThreadCommand, ThreadEvent, ThreadState>(
				system, new ThreadEntity(), DEFAULT_THREAD_ID);
	}

	protected void assertNoIssues() {
		if (!driver.getAllIssues().isEmpty()) {
			driver.getAllIssues().forEach(System.out::println);
			fail("There were issues " + driver.getAllIssues().get(0));
		}
	}

	protected ThreadState createThread(String creator, String... participants) {
		Outcome<ThreadEvent, ThreadState> creationResult = driver.run(
				CreateThread.builder()
						.creatorId(creator)
						.name("thread for " + creator)
						.addParticipants(participants)
						.build());
		assertNoIssues();
		return creationResult.state();
	}

}

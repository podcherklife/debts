package name.li.debts.threads.impl;

import static com.google.common.truth.Truth.assertThat;
import static com.lightbend.lagom.javadsl.testkit.ServiceTest.defaultSetup;
import static com.lightbend.lagom.javadsl.testkit.ServiceTest.withServer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;

import org.junit.Test;

import name.li.debts.commons.security.AuthenticatedDebtPrincipal;
import name.li.debts.commons.security.Insecurity;
import name.li.debts.commons.security.Security;
import name.li.debts.commons.security.TokenDecoder;
import name.li.debts.commons.security.TokenGenerator;
import name.li.debts.commons.security.TokenValidator;
import name.li.debts.commons.test.Tests;
import name.li.debts.threads.api.AddParticipantsRequest;
import name.li.debts.threads.api.CreateThreadRequest;
import name.li.debts.threads.api.ThreadDetails;
import name.li.debts.threads.api.ThreadService;
import name.li.debts.threads.api.ThreadStatus;
import name.li.debts.threads.api.ThreadSummary;

public class ThreadServiceTest {

	TokenGenerator tokenGenerator = Insecurity.INSECURE_TOKEN_GENERATOR();
	TokenDecoder tokenDecoder = new TokenDecoder();
	TokenValidator tokenValidator = Insecurity.INSECURE_TOKEN_VALIDATOR();

	Function<String, AuthenticatedDebtPrincipal> generatePrincipal = user -> {
		String rawToken = tokenGenerator.generateToken(user);
		return tokenDecoder.decodePrincipal(rawToken).validate(tokenValidator);
	};

	AuthenticatedDebtPrincipal john = generatePrincipal.apply("John");

	@Test
	public void shouldCreateThread() {
		withServer(defaultSetup().withCassandra(), server -> {
			ThreadService service = server.client(ThreadService.class);

			String id = Tests.get(
					service.createThread()
							.handleRequestHeader(Security.authenticated(john))
							.invoke(CreateThreadRequest.builder()
									.addParticipants("user1")
									.name("name")
									.description("description")
									.build()));

			assertThat(id).isNotNull();

			ThreadDetails threadDetails = Tests.get(
					service.getThreadDetails(id)
							.handleRequestHeader(Security.authenticated(john))
							.invoke());

			assertThat(threadDetails).isEqualTo(ThreadDetails.builder()
					.id(id)
					.name("name")
					.description("description")
					.addParticipants(john.getName(), "user1")
					.status(ThreadStatus.OPEN)
					.build());

		});
	}

	@Test
	public void shouldAddParticipants() {
		withServer(defaultSetup().withCassandra(), server -> {
			ThreadService service = server.client(ThreadService.class);

			String id = Tests.get(
					service.createThread()
							.handleRequestHeader(Security.authenticated(john))
							.invoke(CreateThreadRequest.builder()
									.name("name")
									.description("description")
									.build()));
			Tests.get(service.addParticipants(id)
					.handleRequestHeader(Security.authenticated(john))
					.invoke(AddParticipantsRequest.builder().addParticipants("participant to add").build()));

			ThreadDetails threadDetails = Tests.get(
					service.getThreadDetails(id)
							.handleRequestHeader(Security.authenticated(john))
							.invoke());

			assertThat(threadDetails.participants()).containsExactly(john.getName(), "participant to add");
		});
	}

	@Test
	public void shouldRemoveParticipants() {
		withServer(defaultSetup().withCassandra(), server -> {
			ThreadService service = server.client(ThreadService.class);

			String id = Tests.get(
					service.createThread()
							.handleRequestHeader(Security.authenticated(john))
							.invoke(CreateThreadRequest.builder()
									.name("name")
									.addParticipants("user1", "user2")
									.description("description")
									.build()));

			Tests.get(service.removeParticipant(id)
					.handleRequestHeader(Security.authenticated(john))
					.invoke("user1"));

			ThreadDetails threadDetails = Tests.get(
					service.getThreadDetails(id)
							.handleRequestHeader(Security.authenticated(john))
							.invoke());

			assertThat(threadDetails.participants()).containsExactly(john.getName(), "user2");
		});
	}

	@Test
	public void testOpenCloseThread() {
		withServer(defaultSetup().withCassandra(), server -> {
			ThreadService service = server.client(ThreadService.class);

			String id = Tests.get(
					service.createThread()
							.handleRequestHeader(Security.authenticated(john))
							.invoke(CreateThreadRequest.builder()
									.name("name")
									.addParticipants("user1", "user2")
									.description("description")
									.build()));

			// close thread
			Tests.get(service.closeThread(id)
					.handleRequestHeader(Security.authenticated(john))
					.invoke());

			ThreadDetails closedThread = Tests.get(
					service.getThreadDetails(id)
							.handleRequestHeader(Security.authenticated(john))
							.invoke());

			assertThat(closedThread.status()).isEqualTo(ThreadStatus.CLOSED);

			// reopen thread
			Tests.get(service.reopenThread(id)
					.handleRequestHeader(Security.authenticated(john))
					.invoke());

			ThreadDetails reopenedThread = Tests.get(
					service.getThreadDetails(id)
							.handleRequestHeader(Security.authenticated(john))
							.invoke());

			assertThat(reopenedThread.status()).isEqualTo(ThreadStatus.OPEN);
		});
	}

	@Test
	public void testGetUserThreads() {
		withServer(defaultSetup().withCassandra(), server -> {
			ThreadService service = server.client(ThreadService.class);

			AuthenticatedDebtPrincipal sally = generatePrincipal.apply("Sally");
			AuthenticatedDebtPrincipal sam = generatePrincipal.apply("Sam");

			String sharedThread = createThreadWithParticipants(service, "sallyAndSamThread", john,
					Arrays.asList(sally.getName(), sam.getName()));
			String sallyThread = createThreadWithParticipants(service, "sallyThread", john,
					Collections.singleton(sally.getName()));
			String samThreadFromWhichSallyWasRemoved = createThreadWithParticipants(service,
					"samThread", john,
					Arrays.asList(sam.getName(), sally.getName()));

			Tests.get(service.removeParticipant(samThreadFromWhichSallyWasRemoved)
					.handleRequestHeader(Security.authenticated(john))
					.invoke(sally.getName()));

			Tests.get(service.closeThread(samThreadFromWhichSallyWasRemoved)
					.handleRequestHeader(Security.authenticated(john))
					.invoke());

			Tests.sleep(10); // wait for readside to process events

			Collection<ThreadSummary> sallyThreads = Tests.get(service.getAllUserThreads()
					.handleRequestHeader(Security.authenticated(sally))
					.invoke());

			assertThat(sallyThreads).containsExactly(
					ThreadSummary.builder()
							.name("sallyAndSamThread")
							.status(ThreadStatus.OPEN)
							.id(sharedThread)
							.build(),
					ThreadSummary.builder()
							.name("sallyThread")
							.status(ThreadStatus.OPEN)
							.id(sallyThread)
							.build());

			Collection<ThreadSummary> samThreads = Tests.get(service.getAllUserThreads()
					.handleRequestHeader(Security.authenticated(sam))
					.invoke());

			assertThat(samThreads).containsExactly(
					ThreadSummary.builder()
							.name("sallyAndSamThread")
							.status(ThreadStatus.OPEN)
							.id(sharedThread)
							.build(),
					ThreadSummary.builder()
							.name("samThread")
							.status(ThreadStatus.CLOSED)
							.id(samThreadFromWhichSallyWasRemoved)
							.build());
		});
	}

	private String createThreadWithParticipants(ThreadService service, String name,
			AuthenticatedDebtPrincipal creator, Collection<String> participants) {
		return Tests.get(
				service.createThread()
						.handleRequestHeader(Security.authenticated(john))
						.invoke(CreateThreadRequest.builder()
								.name(name)
								.participants(participants)
								.description("description")
								.build()));
	}

}

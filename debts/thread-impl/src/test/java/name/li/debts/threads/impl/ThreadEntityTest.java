package name.li.debts.threads.impl;

import static com.google.common.truth.Truth.assertThat;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver.Outcome;
import com.lightbend.lagom.javadsl.testkit.PersistentEntityTestDriver.UnhandledCommand;

public class ThreadEntityTest extends AbstractThreadTest {

	@Test
	public void getStateShouldFailIfThreadWasNotCreated() {
		Outcome<ThreadEvent, ThreadState> result = driver
				.run(GetThreadState.builder().requestor("somebody").build());
		assertThat(result.issues()).hasSize(1);
		assertThat(result.issues().get(0)).isInstanceOf(UnhandledCommand.class);
	}

	@Test
	public void shouldPersistCreationEventAndMaintainState() {
		Outcome<ThreadEvent, ThreadState> creationResult = driver.run(
				CreateThread.builder()
						.creatorId("creator")
						.name("some name")
						.description("description")
						.participants(Collections.singleton("participant"))
						.build());
		assertNoIssues();

		Outcome<ThreadEvent, ThreadState> getStateResult = driver
				.run(GetThreadState.builder().requestor("somebody").build());

		assertThat(getStateResult.issues()).isEmpty();
		assertThat(getStateResult.state())
				.isEqualTo(ThreadState.builder()
						.creator("creator")
						.name("some name")
						.description("description")
						.participants(Arrays.asList("creator", "participant"))
						.build());

		assertNoIssues();
	}

}

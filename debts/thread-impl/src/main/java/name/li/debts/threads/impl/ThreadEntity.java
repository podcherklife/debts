package name.li.debts.threads.impl;

import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

import akka.Done;
import name.li.debts.commons.entity.Exceptions;
import name.li.debts.threads.api.ThreadStatus;

public class ThreadEntity extends PersistentEntity<ThreadCommand, ThreadEvent, ThreadState> {

	@Override
	public PersistentEntity<ThreadCommand, ThreadEvent, ThreadState>.Behavior initialBehavior(
			Optional<ThreadState> snapshotState) {

		PersistentEntity<ThreadCommand, ThreadEvent, ThreadState>.BehaviorBuilder bb = newBehaviorBuilder(
				ThreadState.builder()
						.creator("")
						.name("")
						.description("")
						.build());

		bb.setCommandHandler(CreateThread.class, (cmd, ctx) -> {
			return ctx.thenPersist(ThreadCreated.builder()
					.id(entityId())
					.name(cmd.name())
					.description(cmd.description())
					.creatorId(cmd.creatorId())
					.participants(cmd.participants())
					.build(), (c) -> ctx.reply(entityId()));
		});

		bb.setEventHandlerChangingBehavior(ThreadCreated.class, evt -> {
			return activeThreadBehavior(ThreadState.builder()
					.name(evt.name())
					.description(evt.description())
					.creator(evt.creatorId())
					.participants(ImmutableSet.<String>builder()
							.addAll(evt.participants())
							.add(evt.creatorId()).build())
					.build());
		});

		return bb.build();
	}

	public Behavior activeThreadBehavior(
			ThreadState newState) {
		PersistentEntity<ThreadCommand, ThreadEvent, ThreadState>.BehaviorBuilder bb = newBehaviorBuilder(newState);

		replyOnGetState(bb);

		bb.setCommandHandler(AddParticipants.class, (cmd, ctx) -> {
			if (!state().participants().contains(cmd.requestor())) {
				ctx.commandFailed(new Exceptions.UnauthorizedCommandException(
						"Only existing participant can add new participants"));
				return ctx.done();
			}
			SetView<String> participantsToAdd = Sets.difference(cmd.participants(), state().participants());
			if (participantsToAdd.isEmpty()) {
				return ctx.done(); // https://github.com/lagom/lagom/issues/1089
			} else {
				return ctx.thenPersistAll(
						participantsToAdd.stream()
								.map(p -> ParticipantAdded.builder()
										.threadId(entityId())
										.addedBy(cmd.requestor())
										.participant(p)
										.readSide(ParticipantAddedReadSide.builder()
												.threadName(state().name())
												.threadStatus(state().status())
												.build())
										.build())
								.collect(Collectors.toList()),
						() -> {
							ctx.reply(Done.done());
						});
			}
		});

		bb.setEventHandler(ParticipantAdded.class, evt -> {
			return state().withParticipants(
					ImmutableSet.<String>builder()
							.addAll(state().participants())
							.add(evt.participant())
							.build());
		});

		bb.setCommandHandler(RemoveParticipant.class, (cmd, ctx) -> {
			if (!state().participants().contains(cmd.participantToRemove())) {
				ctx.commandFailed(new Exceptions.IllegalCommandException("Can remove only participants"));
				return ctx.done();
			}
			boolean requestorIsCreator = cmd.requestor().equals(state().creator());
			boolean requestroRemovingSelf = cmd.requestor().equals(cmd.participantToRemove());
			if (!requestorIsCreator && !requestroRemovingSelf) {
				ctx.commandFailed(new Exceptions.UnauthorizedCommandException(
						"Only creator can remove participants other thatn self"));
				return ctx.done();
			}
			return ctx.thenPersist(
					ParticipantRemoved.builder()
							.threadId(entityId())
							.participant(cmd.participantToRemove())
							.removedBy(cmd.requestor())
							.build(),
					(notUsed) -> ctx.reply(Done.done()));
		});

		bb.setEventHandler(ParticipantRemoved.class, (evt) -> {
			return state().withParticipants(
					Sets.difference(state().participants(), ImmutableSet.of(evt.participant())));
		});

		bb.setCommandHandler(CloseThread.class, (cmd, ctx) -> {
			if (!cmd.requestor().equals(state().creator())) {
				ctx.commandFailed(new Exceptions.UnauthorizedCommandException("Only creator can close thread"));
				return ctx.done();
			}
			return ctx.thenPersist(ThreadClosed.builder()
					.threadId(entityId())
					.closedBy(cmd.requestor()).build(),
					(evt) -> ctx.reply(Done.done()));
		});

		bb.setEventHandlerChangingBehavior(ThreadClosed.class,
				evt -> closedThreadBehavior(state().withStatus(ThreadStatus.CLOSED)));

		bb.setCommandHandler(ReopenThread.class, (cmd, ctx) -> {
			ctx.commandFailed(new Exceptions.IllegalCommandException("Thread is already open and cannot be reopened"));
			return ctx.done();
		});

		return bb.build();
	}

	public Behavior closedThreadBehavior(ThreadState newState) {
		PersistentEntity<ThreadCommand, ThreadEvent, ThreadState>.BehaviorBuilder bb = newBehaviorBuilder(
				state().withStatus(ThreadStatus.CLOSED));

		replyOnGetState(bb);

		bb.setCommandHandler(CloseThread.class, (cmd, ctx) -> {
			ctx.commandFailed(new Exceptions.IllegalCommandException("Thread is already closed"));
			return ctx.done();
		});

		bb.setCommandHandler(ReopenThread.class, (cmd, ctx) -> {
			if (!cmd.requestor().equals(state().creator())) {
				ctx.commandFailed(new Exceptions.UnauthorizedCommandException("Only creator can reopen thread"));
				return ctx.done();
			}
			return ctx.thenPersist(
					ThreadReopened.builder()
							.threadId(entityId())
							.reopenedBy(cmd.requestor())
							.build(),
					evt -> ctx.reply(Done.done()));
		});

		bb.setEventHandlerChangingBehavior(ThreadReopened.class,
				evt -> activeThreadBehavior(state().withStatus(ThreadStatus.OPEN)));

		return bb.build();
	}

	public void replyOnGetState(BehaviorBuilder bb) {
		bb.setReadOnlyCommandHandler(GetThreadState.class, (cmd, ctx) -> {
			ctx.reply(state());
		});
	}

}
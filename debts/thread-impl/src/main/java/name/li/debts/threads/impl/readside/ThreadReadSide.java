package name.li.debts.threads.impl.readside;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.pcollections.PSequence;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSide;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;

import akka.Done;
import name.li.debts.threads.api.ThreadStatus;
import name.li.debts.threads.api.ThreadSummary;
import name.li.debts.threads.impl.ParticipantAdded;
import name.li.debts.threads.impl.ParticipantRemoved;
import name.li.debts.threads.impl.ThreadClosed;
import name.li.debts.threads.impl.ThreadCreated;
import name.li.debts.threads.impl.ThreadEvent;
import name.li.debts.threads.impl.ThreadReopened;

public class ThreadReadSide {

	private CassandraSession cassandra;

	@Inject
	public ThreadReadSide(CassandraSession cassandra) {
		this.cassandra = cassandra;
	}

	public CompletionStage<Collection<ThreadSummary>> getThreadsForUser(String userId) {
		return cassandra
				.prepare(
						"SELECT threadId, threadName, threadStatus FROM userThreads WHERE interestedUser = ?")
				.thenCompose(st -> {
					return cassandra.selectAll(st.bind(userId))
							.thenApply(
									rows -> rows.stream().map(
											row -> ThreadSummary.builder()
													.id(row.getString("threadId"))
													.name(row.getString("threadName"))
													.status(ThreadStatus.valueOf(row.getString("threadStatus")))
													.build())
											.collect(Collectors.toList()));
				});
	}

	public static class ThreadEventProcessor extends ReadSideProcessor<ThreadEvent> {

		private final CassandraSession cassandra;
		private final CassandraReadSide readSide;
		private PreparedStatement addThreadToUser;
		private PreparedStatement removeThreadFromUser;
		private PreparedStatement updateStatus;
		private PreparedStatement selectUsersForThread;

		@Inject
		public ThreadEventProcessor(CassandraSession cassandra, CassandraReadSide readSide) {
			this.cassandra = cassandra;
			this.readSide = readSide;
		}

		@Override
		public PSequence<AggregateEventTag<ThreadEvent>> aggregateTags() {
			return ThreadEvent.TAG.allTags();
		}

		@Override
		public ReadSideProcessor.ReadSideHandler<ThreadEvent> buildHandler() {
			return readSide
					.<ThreadEvent>builder("threadReadSide")
					.setGlobalPrepare(this::createTables)
					.setPrepare(this::prepare)
					.setEventHandler(ThreadCreated.class, this::handleThreadCreated)
					.setEventHandler(ParticipantAdded.class, this::handleParticipantAdded)
					.setEventHandler(ParticipantRemoved.class, this::handleParticipantRemoved)
					.setEventHandler(ThreadClosed.class, this::handleThreadClosed)
					.setEventHandler(ThreadReopened.class, this::handleThreadReopened)
					.build();
		}

		private CompletionStage<Done> createTables() {
			return cassandra.executeCreateTable(
					"CREATE TABLE IF NOT EXISTS userThreads("
							+ "interestedUser text, "
							+ "threadId text, "
							+ "threadName text, "
							+ "creationDate timestamp, "
							+ "threadStatus text, "
							+ "PRIMARY KEY (interestedUser, threadId)"
							+ ")")
					.thenCompose(notUsed -> cassandra.executeCreateTable(
							"CREATE MATERIALIZED VIEW threadUsers "
									+ "AS SELECT interestedUser, threadId FROM userThreads "
									+ "WHERE interestedUser IS NOT NULL AND threadId IS NOT NULL "
									+ "PRIMARY KEY (threadId, interestedUser)"));
		}

		private CompletionStage<Done> prepare(AggregateEventTag<ThreadEvent> tag) {
			return CompletableFuture.allOf(
					prepareAddThreadToUser(),
					prepareUpdateThreadStatus(),
					prepareRemoveThreadTromUser(),
					prepareSelectUsersForThread())
					.thenApply(notUsed -> Done.done());
		}

		private CompletableFuture<List<BoundStatement>> handleThreadReopened(ThreadReopened event) {
			return updateThreadStatus(event.threadId(), ThreadStatus.OPEN)
					.thenApply(stm -> Arrays.asList(stm))
					.toCompletableFuture();
		}

		private CompletableFuture<List<BoundStatement>> handleThreadClosed(ThreadClosed event) {
			return updateThreadStatus(event.threadId(), ThreadStatus.CLOSED)
					.thenApply(stm -> Arrays.asList(stm))
					.toCompletableFuture();
		}

		private CompletableFuture<List<BoundStatement>> handleParticipantRemoved(ParticipantRemoved event) {
			return CompletableFuture.completedFuture(Collections.singletonList(
					removeThreadFromUser.bind(event.participant(), event.threadId())));
		}

		private CompletableFuture<List<BoundStatement>> handleParticipantAdded(ParticipantAdded event) {
			return CompletableFuture.completedFuture(Collections.singletonList(addThreadToUser(event.threadId(),
					event.participant(), event.readSide().threadName(), event.readSide().threadStatus())));
		}

		private CompletableFuture<List<BoundStatement>> handleThreadCreated(ThreadCreated event) {
			return CompletableFuture.completedFuture(event.participants().stream()
					.map(user -> addThreadToUser(event.id(), user, event.name(), ThreadStatus.OPEN))
					.collect(Collectors.toList()));
		}

		private BoundStatement addThreadToUser(
				String threadId, String user, String threadName, ThreadStatus threadStatus) {
			return addThreadToUser.bind(user, threadId, threadName, new Date(), threadStatus.toString());
		}

		private CompletableFuture<BoundStatement> updateThreadStatus(String threadId, ThreadStatus threadStatus) {
			return cassandra.selectAll(selectUsersForThread.bind(threadId))
					.thenApply(rows -> rows.stream().map(row -> row.getString("interestedUser"))
							.collect(Collectors.toList()))
					.thenApply(ids -> updateStatus.bind(threadStatus.toString(), ids, threadId))
					.toCompletableFuture();
		}

		private CompletableFuture<Done> prepareAddThreadToUser() {
			return prepareStatement(
					"INSERT INTO userThreads(interestedUser, threadId, threadName, creationDate, threadStatus) VALUES (?,?,?,?,?)",
					stm -> this.addThreadToUser = stm);
		}

		private CompletableFuture<Done> prepareRemoveThreadTromUser() {
			return prepareStatement("DELETE FROM userThreads WHERE interestedUser =  ? AND threadId = ?",
					stm -> this.removeThreadFromUser = stm);
		}

		private CompletableFuture<Done> prepareUpdateThreadStatus() {
			return prepareStatement(
					"UPDATE userThreads SET threadStatus = ? WHERE interestedUser IN ? AND threadId = ?",
					stm -> this.updateStatus = stm);
		}

		private CompletableFuture<Done> prepareSelectUsersForThread() {
			return prepareStatement(
					"SELECT interestedUser FROM threadUsers WHERE threadId = ?",
					(stm) -> this.selectUsersForThread = stm);
		}

		private CompletableFuture<Done> prepareStatement(String statement,
				Consumer<PreparedStatement> doWithStatement) {
			return cassandra.prepare(statement)
					.thenApply(stm -> {
						doWithStatement.accept(stm);
						return Done.done();
					}).toCompletableFuture();
		}

	}

}

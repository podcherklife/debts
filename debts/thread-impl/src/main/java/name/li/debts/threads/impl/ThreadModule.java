package name.li.debts.threads.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;

import name.li.debts.commons.security.Insecurity;
import name.li.debts.commons.security.TokenDecoder;
import name.li.debts.commons.security.TokenValidator;
import name.li.debts.threads.api.ThreadService;

public class ThreadModule extends AbstractModule implements ServiceGuiceSupport {

	@Override
	protected void configure() {
		bindService(ThreadService.class, ThreadServiceImpl.class);
		bind(TokenValidator.class).toInstance(Insecurity.INSECURE_TOKEN_VALIDATOR());
	}
}

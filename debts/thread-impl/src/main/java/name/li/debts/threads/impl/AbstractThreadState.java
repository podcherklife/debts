package name.li.debts.threads.impl;

import java.util.Collections;
import java.util.Set;

import org.immutables.value.Value.Default;
import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.serialization.Jsonable;

import name.li.debts.commons.ImmutableStyle;
import name.li.debts.threads.api.ThreadStatus;

@JsonDeserialize
@Immutable
@ImmutableStyle
public interface AbstractThreadState extends Jsonable {

	String creator();

	String name();

	String description();

	@Default
	default Set<String> participants() {
		return Collections.emptySet();
	}

	@Default
	default ThreadStatus status() {
		return ThreadStatus.OPEN;
	}

}

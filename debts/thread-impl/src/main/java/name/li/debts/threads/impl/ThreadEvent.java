package name.li.debts.threads.impl;

import java.util.Set;

import org.immutables.value.Value.Auxiliary;
import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import com.lightbend.lagom.serialization.Jsonable;

import name.li.debts.commons.ImmutableStyle;
import name.li.debts.threads.api.ThreadStatus;

public interface ThreadEvent extends Jsonable, AggregateEvent<ThreadEvent> {

	AggregateEventShards<ThreadEvent> TAG = AggregateEventTag.sharded(ThreadEvent.class, 4);

	@Override
	default AggregateEventTagger<ThreadEvent> aggregateTag() {
		return TAG;
	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	public interface AbstractThreadCreated extends ThreadEvent {

		String creatorId();

		String id();

		String name();

		String description();

		Set<String> participants();

	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	public interface AbstractParticipantAdded extends ThreadEvent {

		String threadId();

		String addedBy();

		String participant();

		@Auxiliary
		ParticipantAddedReadSide readSide();

		@JsonDeserialize
		@Immutable
		@ImmutableStyle
		public interface AbstractParticipantAddedReadSide {
			ThreadStatus threadStatus();

			String threadName();
		}
	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	public interface AbstractParticipantRemoved extends ThreadEvent {

		String threadId();

		String removedBy();

		String participant();
	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	public interface AbstractThreadClosed extends ThreadEvent {

		String threadId();

		String closedBy();

	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	public interface AbstractThreadReopened extends ThreadEvent {

		String threadId();

		String reopenedBy();

	}

}

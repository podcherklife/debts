package name.li.debts.threads.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

import com.google.inject.Inject;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;
import com.lightbend.lagom.javadsl.persistence.ReadSide;

import akka.Done;
import akka.NotUsed;
import name.li.debts.commons.security.Security;
import name.li.debts.commons.security.TokenValidator;
import name.li.debts.threads.api.AddParticipantsRequest;
import name.li.debts.threads.api.CreateThreadRequest;
import name.li.debts.threads.api.ThreadDetails;
import name.li.debts.threads.api.ThreadService;
import name.li.debts.threads.api.ThreadSummary;
import name.li.debts.threads.impl.readside.ThreadReadSide;

public class ThreadServiceImpl implements ThreadService {

	private final TokenValidator tokenValidator;
	private PersistentEntityRegistry entityRegistry;
	private ThreadReadSide threadReadSide;

	@Inject
	public ThreadServiceImpl(
			TokenValidator tokenValidator,
			PersistentEntityRegistry entityRegistry,
			ReadSide readSide,
			ThreadReadSide threadReadSide) {
		this.tokenValidator = tokenValidator;
		this.entityRegistry = entityRegistry;
		this.threadReadSide = threadReadSide;

		entityRegistry.register(ThreadEntity.class);
		readSide.register(ThreadReadSide.ThreadEventProcessor.class);
	}

	@Override
	public ServiceCall<CreateThreadRequest, String> createThread() {
		return Security.authenticated(tokenValidator, (principal, req) -> {
			return entityRegistry.refFor(ThreadEntity.class, UUID.randomUUID().toString())
					.ask(CreateThread.builder()
							.name(req.name())
							.participants(req.participants())
							.description(req.description())
							.creatorId(principal.getName())
							.build());
		});
	}

	@Override
	public ServiceCall<NotUsed, ThreadDetails> getThreadDetails(String threadId) {
		return Security.authenticated(tokenValidator, (principal, req) -> {
			return entityRegistry.refFor(ThreadEntity.class, threadId)
					.ask(GetThreadState.builder().requestor(principal.getName()).build())
					.thenApply(state -> ThreadDetails.builder()
							.id(threadId)
							.name(state.name())
							.description(state.description())
							.participants(state.participants())
							.records(Collections.emptyList())
							.status(state.status())
							.build());
		});
	}

	@Override
	public ServiceCall<AddParticipantsRequest, Done> addParticipants(String threadId) {
		return Security.authenticated(tokenValidator, (principal, req) -> {
			return entityRegistry.refFor(ThreadEntity.class, threadId)
					.ask(AddParticipants.builder()
							.requestor(principal.getName())
							.participants(req.participants())
							.build());
		});
	}

	@Override
	public ServiceCall<String, Done> removeParticipant(String threadId) {
		return Security.authenticated(tokenValidator, (principal, req) -> {
			return entityRegistry.refFor(ThreadEntity.class, threadId)
					.ask(RemoveParticipant.builder()
							.requestor(principal.getName())
							.participantToRemove(req)
							.build());
		});
	}

	@Override
	public ServiceCall<NotUsed, Done> reopenThread(String threadId) {
		return Security.authenticated(tokenValidator, (principal, req) -> {
			return entityRegistry.refFor(ThreadEntity.class, threadId)
					.ask(ReopenThread.builder()
							.requestor(principal.getName())
							.build());
		});
	}

	@Override
	public ServiceCall<NotUsed, Done> closeThread(String threadId) {
		return Security.authenticated(tokenValidator, (principal, req) -> {
			return entityRegistry.refFor(ThreadEntity.class, threadId)
					.ask(CloseThread.builder()
							.requestor(principal.getName())
							.build());
		});
	}

	@Override
	public ServiceCall<NotUsed, Collection<ThreadSummary>> getAllUserThreads() {
		return Security.authenticated(tokenValidator, (principal, req) -> {
			return threadReadSide.getThreadsForUser(principal.getName());
		});
	}
}

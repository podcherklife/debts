package name.li.debts.threads.impl;

import java.util.Collections;
import java.util.Set;

import org.immutables.value.Value.Default;
import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import com.lightbend.lagom.serialization.Jsonable;

import akka.Done;
import name.li.debts.commons.ImmutableStyle;

public interface ThreadCommand extends Jsonable {

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractCreateThread extends ThreadCommand, PersistentEntity.ReplyType<String> {
		String name();

		@Default
		default String description() {
			return "";
		}

		String creatorId();

		@Default
		default Set<String> participants() {
			return Collections.emptySet();
		}
	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractGetThreadState extends ThreadCommand, PersistentEntity.ReplyType<ThreadState> {

		String requestor();

	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractAddParticipants extends ThreadCommand, PersistentEntity.ReplyType<Done> {

		String requestor();

		Set<String> participants();

	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractRemoveParticipant extends ThreadCommand, PersistentEntity.ReplyType<Done> {

		String requestor();

		String participantToRemove();

	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractCloseThread extends ThreadCommand, PersistentEntity.ReplyType<Done> {
		String requestor();
	}

	@JsonDeserialize
	@Immutable
	@ImmutableStyle
	interface AbstractReopenThread extends ThreadCommand, PersistentEntity.ReplyType<Done> {
		String requestor();
	}

}

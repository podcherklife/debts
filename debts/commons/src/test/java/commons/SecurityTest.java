package commons;

import static com.google.common.truth.Truth.assertThat;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.BiFunction;

import org.junit.Test;

import com.lightbend.lagom.javadsl.api.transport.RequestHeader;
import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;
import com.lightbend.lagom.javadsl.api.transport.TransportException;

import akka.NotUsed;
import name.li.debts.commons.security.AuthenticatedDebtPrincipal;
import name.li.debts.commons.security.Insecurity;
import name.li.debts.commons.security.Security;
import name.li.debts.commons.security.TokenDecoder;
import name.li.debts.commons.security.TokenGenerator;
import name.li.debts.commons.security.TokenValidator;
import name.li.debts.commons.security.UnauthenticatedDebtPrincipal;

public class SecurityTest {

	private TokenValidator validator = Insecurity.INSECURE_TOKEN_VALIDATOR();
	private TokenGenerator generator = Insecurity.INSECURE_TOKEN_GENERATOR();
	private TokenDecoder decoder = new TokenDecoder();

	private RequestStub requestStub = new RequestStub();

	@Test
	public void testAllowRequestsWithValidButNotValidatedYetToken() {
		Security.authenticated(validator, requestStub::call)
				.invokeWithHeaders(RequestHeader.DEFAULT.withPrincipal(
						nonValidatedPrincipal()),
						NotUsed.notUsed());

		assertThat(requestStub.wasCalled()).isTrue();
	}

	@Test
	public void testAllowRequestsWithValidButNotValidToken() {
		Security.authenticated(validator, requestStub::call)
				.invokeWithHeaders(RequestHeader.DEFAULT.withPrincipal(
						validatedPrincipal()),
						NotUsed.notUsed());

		assertThat(requestStub.wasCalled()).isTrue();
	}

	@Test
	public void testAllowsAuthenticatedRequests() {
		TokenValidator validator = Insecurity.INSECURE_TOKEN_VALIDATOR();
		TokenGenerator generator = Insecurity.INSECURE_TOKEN_GENERATOR();

		RequestStub stub = new RequestStub();

		Security.authenticated(validator, stub::call)
				.invokeWithHeaders(RequestHeader.DEFAULT.withPrincipal(
						new TokenDecoder().decodePrincipal(generator.generateToken("someUser"))), NotUsed.notUsed());

		assertThat(stub.wasCalled()).isTrue();
	}

	@Test
	public void testDenyNonAuthenticatedRequests() {
		TokenValidator validator = Insecurity.INSECURE_TOKEN_VALIDATOR();

		RequestStub stub = new RequestStub();

		try {
			Security.authenticated(validator, stub::call).invokeWithHeaders(RequestHeader.DEFAULT, NotUsed.notUsed());
		} catch (TransportException e) {
			assertThat(e.errorCode()).isEqualTo(TransportErrorCode.fromHttp(401));
		}

		assertThat(stub.wasCalled()).isFalse();

	}

	private UnauthenticatedDebtPrincipal nonValidatedPrincipal() {
		return new TokenDecoder().decodePrincipal(generator.generateToken("someUser"));
	}

	private AuthenticatedDebtPrincipal validatedPrincipal() {
		UnauthenticatedDebtPrincipal nonValidatedPrincipal = nonValidatedPrincipal();
		return new AuthenticatedDebtPrincipal(validator.validate(nonValidatedPrincipal.getDecodedToken()),
				nonValidatedPrincipal.getRawToken());
	}

	private static class RequestStub {

		private boolean wasCalled = false;

		public <Req, Res> CompletionStage<Res> call(AuthenticatedDebtPrincipal principal, Req request) {
			wasCalled = true;
			return CompletableFuture.completedFuture(null);
		}

		public boolean wasCalled() {
			return wasCalled;
		}

	}

	private static <Req, Res> BiFunction<AuthenticatedDebtPrincipal, Req, CompletionStage<Res>> requestMock() {
		return null;
	}

}

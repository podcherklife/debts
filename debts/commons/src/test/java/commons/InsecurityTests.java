package commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import name.li.debts.commons.security.Insecurity;

public class InsecurityTests {

	@Test
	public void shouldGenerateSameKeysForSameSeed() {
		assertEquals(
				Insecurity.INSECURE_KEY_PAIR_GENERATOR(1).generateKeyPair().getPublic(),
				Insecurity.INSECURE_KEY_PAIR_GENERATOR(1).generateKeyPair().getPublic());
	}

	@Test
	public void shouldGenerateDifferentKeysForSameDifferentSeeds() {
		assertNotEquals(
				Insecurity.INSECURE_KEY_PAIR_GENERATOR(1).generateKeyPair().getPublic(),
				Insecurity.INSECURE_KEY_PAIR_GENERATOR(2).generateKeyPair().getPublic());
	}
}

package commons;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.fail;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import org.junit.Test;

import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import name.li.debts.commons.security.Insecurity;
import name.li.debts.commons.security.TokenDecoder;
import name.li.debts.commons.security.TokenGenerator;
import name.li.debts.commons.security.TokenValidator;

public class TokenTests {

	@Test
	public void sameSignAndValidateSecrets() {
		KeyPairGenerator kpg = Insecurity.INSECURE_KEY_PAIR_GENERATOR(100);
		KeyPair kp = kpg.generateKeyPair();

		TokenGenerator generator = new TokenGenerator((RSAPrivateKey) kp.getPrivate());
		TokenValidator validator = new TokenValidator((RSAPublicKey) kp.getPublic());
		TokenDecoder decoder = new TokenDecoder();

		String user = "user";

		String token = generator.generateToken(user);
		DecodedJWT decodedToken = decoder.decodePrincipal(token).validate(validator).getJwtToken();

		assertThat(decodedToken.getSubject()).isEqualTo(user);
	}

	@Test
	public void differentSignAndValidateSecrets() {
		TokenGenerator generator = new TokenGenerator(
				(RSAPrivateKey) Insecurity.INSECURE_KEY_PAIR_GENERATOR(1).generateKeyPair().getPrivate());
		TokenValidator validator = new TokenValidator(
				(RSAPublicKey) Insecurity.INSECURE_KEY_PAIR_GENERATOR(2).generateKeyPair().getPublic());
		TokenDecoder decoder = new TokenDecoder();

		String user = "user";

		try {
			String token = generator.generateToken(user);
			validator.validate(decoder.decodePrincipal(token).validate(validator).getJwtToken());

			fail("expected exception");
		} catch (Exception e) {
			assertThat(e).isInstanceOf(SignatureVerificationException.class);
		}
	}

	@Test
	public void decodeTokenWithoutVerifyingSignature() {
		TokenGenerator generator = new TokenGenerator(
				(RSAPrivateKey) Insecurity.INSECURE_KEY_PAIR_GENERATOR(/* just a random number */ 123).generateKeyPair()
						.getPrivate());

		String user = "subject";
		String token = generator.generateToken(user);

		DecodedJWT decodedToken = new TokenDecoder().decodePrincipal(token).getDecodedToken();
		assertThat(decodedToken.getSubject()).isEqualTo(user);
	}

	@Test
	public void invalidSignatureFailsTokenValidation() {
		KeyPairGenerator kpg = Insecurity.INSECURE_KEY_PAIR_GENERATOR(1);
		KeyPair kp = kpg.generateKeyPair();

		TokenGenerator generator = new TokenGenerator((RSAPrivateKey) kp.getPrivate());
		TokenValidator validator = new TokenValidator((RSAPublicKey) kp.getPublic());

		String user = "subject";
		String token = generator.generateToken(user);

		// token format is <head>.<payload>.<signature>
		String tokenWithModifiedSignature = token.substring(0, token.lastIndexOf('.') + 1) + "aaaaaaaaaa";

		DecodedJWT decodedTokenWithModifiedSignature = new TokenDecoder().decodePrincipal(tokenWithModifiedSignature)
				.getDecodedToken();

		try {
			validator.validate(decodedTokenWithModifiedSignature);
			fail("Expected validation error");
		} catch (SignatureVerificationException e) {
			assertThat(e).isNotNull();
		}

	}

}

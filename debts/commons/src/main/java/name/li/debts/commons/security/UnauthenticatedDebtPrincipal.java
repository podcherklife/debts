package name.li.debts.commons.security;

import java.security.Principal;

import com.auth0.jwt.interfaces.DecodedJWT;

public class UnauthenticatedDebtPrincipal implements Principal {

	private DecodedJWT decodedToken;

	// XXX needed for easy propagation
	private String rawToken;

	public UnauthenticatedDebtPrincipal(DecodedJWT decodedToken, String rawToken) {
		this.decodedToken = decodedToken;
		this.rawToken = rawToken;
	}

	@Override
	public String getName() {
		return decodedToken.getSubject();
	}

	public String getRawToken() {
		return rawToken;
	}

	public AuthenticatedDebtPrincipal validate(TokenValidator validator) {
		return new AuthenticatedDebtPrincipal(validator.validate(decodedToken), rawToken);
	}

	public DecodedJWT getDecodedToken() {
		return decodedToken;
	}

}

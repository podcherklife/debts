package name.li.debts.commons.security;

import java.security.Principal;
import java.util.Optional;

import com.google.common.base.Predicates;
import com.lightbend.lagom.javadsl.api.transport.HeaderFilter;
import com.lightbend.lagom.javadsl.api.transport.RequestHeader;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;

import play.mvc.Http;

public class TokenHeaderFilter implements HeaderFilter {

	public static final String TOKEN_HEADER = "TokenHeader";
	private TokenDecoder tokenDecoder;

	public TokenHeaderFilter(TokenDecoder tokenDecoder) {
		this.tokenDecoder = tokenDecoder;
	}

	@Override
	public RequestHeader transformClientRequest(RequestHeader request) {
		Optional<Principal> mayBePrincipal = request.principal()
				.filter(Predicates.instanceOf(AuthenticatedDebtPrincipal.class));
		if (mayBePrincipal.isPresent()) {
			AuthenticatedDebtPrincipal dp = (AuthenticatedDebtPrincipal) mayBePrincipal.get();
			return Security.authenticated(dp).apply(request);
		} else {
			return request;
		}
	}

	@Override
	public RequestHeader transformServerRequest(RequestHeader request) {
		Optional<String> mayBeToken = request
				.getHeader(Http.HeaderNames.AUTHORIZATION)
				.filter(str -> str.startsWith(Security.BEARER_PREFIX))
				.map(str -> str.substring(Security.BEARER_PREFIX.length()));
		if (mayBeToken.isPresent()) {
			String rawToken = mayBeToken.get();
			return request.withPrincipal(tokenDecoder.decodePrincipal(rawToken));
		} else {
			return request;
		}
	}

	@Override
	public ResponseHeader transformServerResponse(ResponseHeader response, RequestHeader request) {
		return response;
	}

	@Override
	public ResponseHeader transformClientResponse(ResponseHeader response, RequestHeader request) {
		return response;
	}

}

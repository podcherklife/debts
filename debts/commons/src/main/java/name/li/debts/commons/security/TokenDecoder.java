package name.li.debts.commons.security;

import com.auth0.jwt.JWT;

public class TokenDecoder {

	public UnauthenticatedDebtPrincipal decodePrincipal(String token) {
		return new UnauthenticatedDebtPrincipal(JWT.decode(token), token);
	}

}

package name.li.debts.commons.security;

import java.security.Principal;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.function.BiFunction;
import java.util.function.Function;

import com.lightbend.lagom.javadsl.api.deser.ExceptionMessage;
import com.lightbend.lagom.javadsl.api.transport.RequestHeader;
import com.lightbend.lagom.javadsl.api.transport.ResponseHeader;
import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;
import com.lightbend.lagom.javadsl.api.transport.TransportException;
import com.lightbend.lagom.javadsl.server.HeaderServiceCall;

import akka.japi.Pair;
import play.mvc.Http;

public class Security {
	static final String BEARER_PREFIX = "Bearer ";

	public static <Req, Res> HeaderServiceCall<Req, Res> authenticated(TokenValidator tokenValidator,
			BiFunction<AuthenticatedDebtPrincipal, Req, CompletionStage<Res>> call) {
		return (header, req) -> {
			Optional<Principal> mayBePrincipal = header.principal();
			if (mayBePrincipal.isPresent()) {
				Principal principal = mayBePrincipal.get();
				if (principal instanceof AuthenticatedDebtPrincipal) {
					return call.apply((AuthenticatedDebtPrincipal) principal, req)
							.thenApply(res -> Pair.create(ResponseHeader.OK, res));
				} else if (principal instanceof UnauthenticatedDebtPrincipal) {
					return call.apply(((UnauthenticatedDebtPrincipal) principal).validate(tokenValidator), req)
							.thenApply(res -> Pair.create(ResponseHeader.OK, res));
				} else {
					throw TransportException.fromCodeAndMessage(TransportErrorCode.Forbidden,
							new ExceptionMessage("Authentication error", "Unexpected principal: " + principal));
				}
			} else {
				throw new TransportException(TransportErrorCode.fromHttp(401),
						new ExceptionMessage("Authentication error", "Authentication required"));
			}
		};
	}

	public static Function<RequestHeader, RequestHeader> authenticated(AuthenticatedDebtPrincipal principal) {
		return request -> {
			return request.withHeader(Http.HeaderNames.AUTHORIZATION,
					BEARER_PREFIX + principal.getRawToken());
		};
	}

}

package name.li.debts.commons.security;

import java.security.interfaces.RSAPrivateKey;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

public class TokenGenerator {

	private Algorithm algorithm;

	public TokenGenerator(RSAPrivateKey key) {
		this.algorithm = Algorithm.RSA256(null, key);
	}

	public String generateToken(String user) {
		return JWT.create()
				.withIssuer(issuer())
				.withSubject(user)
				.sign(algorithm);
	}

	public static String issuer() {
		return "debts-app";
	}

}

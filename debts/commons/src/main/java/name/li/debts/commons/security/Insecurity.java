package name.li.debts.commons.security;

import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Random;

@Deprecated
public class Insecurity {

	public static KeyPairGenerator INSECURE_KEY_PAIR_GENERATOR(long seed) {
		Random rnd = new Random(seed);
		KeyPairGenerator kpg;
		try {
			kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(2048, new SecureRandom() {
				private static final long serialVersionUID = 1L;

				@Override
				public void nextBytes(byte[] bytes) {
					rnd.nextBytes(bytes);
				}
			});
			return kpg;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static TokenGenerator INSECURE_TOKEN_GENERATOR() {
		return new TokenGenerator((RSAPrivateKey) INSECURE_KEY_PAIR_GENERATOR(1).generateKeyPair().getPrivate());
	}

	public static TokenValidator INSECURE_TOKEN_VALIDATOR() {
		return new TokenValidator((RSAPublicKey) INSECURE_KEY_PAIR_GENERATOR(1).generateKeyPair().getPublic());
	}

}

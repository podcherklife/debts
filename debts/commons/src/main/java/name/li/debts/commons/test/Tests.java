package name.li.debts.commons.test;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

public class Tests {

	public static class CallResult<T> {
		public static <T> CallResult<T> done(T data) {
			return new Data<T>(data);
		}

		public static <T> CallResult<T> noData() {
			return new NoDataYet<T>();
		}
	}

	static class NoDataYet<T> extends CallResult<T> {

	}

	static class Data<T> extends CallResult<T> {
		private T data;

		public Data(T data) {
			this.data = data;
		}

		public T getData() {
			return data;
		}
	}

	public static void sleep(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static <T> void eventually(Callable<CallResult<T>> supplier, Consumer<T> asserter) {
		int numTries = 20;
		while (numTries-- > 0) {
			try {
				CallResult<T> callResult = supplier.call();
				if (callResult instanceof NoDataYet) {
					System.out.println("No data, tries left: " + numTries);
					sleep(1);
					continue;
				} else if (callResult instanceof Data) {
					asserter.accept(((Data<T>) callResult).getData());
					// success!
					return;
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		throw new IllegalStateException("Did not happen after tries: " + numTries);
	}

	public static <T> T get(CompletionStage<T> promise) {
		try {
			return promise.toCompletableFuture().get(20, TimeUnit.SECONDS);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			throw new RuntimeException(e);
		}
	}
}

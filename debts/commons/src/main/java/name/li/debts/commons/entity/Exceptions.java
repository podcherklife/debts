package name.li.debts.commons.entity;

import com.lightbend.lagom.javadsl.api.transport.TransportErrorCode;
import com.lightbend.lagom.javadsl.api.transport.TransportException;
import com.lightbend.lagom.serialization.Jsonable;

public class Exceptions {

	public static class IllegalCommandException
			extends TransportException implements Jsonable {

		private String message;

		public IllegalCommandException(String message) {
			super(TransportErrorCode.PolicyViolation, message);
			this.message = message;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((message == null) ? 0 : message.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			IllegalCommandException other = (IllegalCommandException) obj;
			if (message == null) {
				if (other.message != null)
					return false;
			} else if (!message.equals(other.message))
				return false;
			return true;
		}

		private static final long serialVersionUID = 1L;

	}

	public static class UnauthorizedCommandException
			extends TransportException implements Jsonable {

		private String message;

		public UnauthorizedCommandException(String message) {
			super(TransportErrorCode.Forbidden, message);
			this.message = message;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((message == null) ? 0 : message.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UnauthorizedCommandException other = (UnauthorizedCommandException) obj;
			if (message == null) {
				if (other.message != null)
					return false;
			} else if (!message.equals(other.message))
				return false;
			return true;
		}

		private static final long serialVersionUID = 1L;

	}
}

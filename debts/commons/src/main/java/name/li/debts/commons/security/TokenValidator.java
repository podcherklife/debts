package name.li.debts.commons.security;

import java.security.interfaces.RSAPublicKey;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

public class TokenValidator {
	private com.auth0.jwt.JWTVerifier verifier;

	public TokenValidator(RSAPublicKey key) {
		this.verifier = JWT.require(Algorithm.RSA256(key, null))
				.withIssuer(TokenGenerator.issuer())
				.build();
	}

	public DecodedJWT validate(DecodedJWT token) {
		return verifier.verify(token);
	}

}

package name.li.debts.identity.api;

import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.pathCall;

import org.immutables.value.Value.Immutable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

import akka.NotUsed;
import name.li.debts.commons.ImmutableStyle;
import name.li.debts.commons.security.TokenDecoder;
import name.li.debts.commons.security.TokenHeaderFilter;

public interface IdentityService extends Service {

	@Immutable
	@ImmutableStyle
	@JsonSerialize
	interface AbstractCreateIdentityRequest {
		String name();

		String password();
	}

	public ServiceCall<CreateIdentityRequest, String> createIdentity();

	@Immutable
	@ImmutableStyle
	@JsonSerialize
	interface AbstractQueryStateResponse {
		String id();

		String name();
	}

	public ServiceCall<NotUsed, QueryStateResponse> getInfo(String id);

	@Immutable
	@ImmutableStyle
	@JsonSerialize
	interface AbstractCheckPasswordRequest {
		String password();
	}

	public ServiceCall<CheckPasswordRequest, Boolean> checkPassword(String id);

	@Immutable
	@ImmutableStyle
	@JsonSerialize
	interface AbstractAcquireTokenRequest {
		String login();

		String password();
	}

	public ServiceCall<AcquireTokenRequest, String> acquireToken();

	@Override
	default Descriptor descriptor() {
		return named("debts")
				.withHeaderFilter(new TokenHeaderFilter(new TokenDecoder()))
				.withCalls(
						pathCall("/api/identity/create", this::createIdentity),
						pathCall("/api/identity/:id", this::getInfo),
						pathCall("/api/identity/:id/checkPassword", this::checkPassword),
						pathCall("/api/identity/acquireToken", this::acquireToken)
								.withAutoAcl(true));
	}
}
